// const Benchmark = require('benchmark');
// const math = require('mathjs')
const pipeline = require('./pipeline');
const { departureProv } = require('./src/tfl')

// Benchmark.options.minTime = -Infinity
// Benchmark.options.maxTime = -Infinity

let iterator = 0

while (true) {
  iterator++
  console.log(iterator)
  const dummyPayload = {
    data: {
      departureId: 300000081,
      destinationId: 300000081
    },
    metadata: departureProv()
  }
  pipeline(dummyPayload)
}
