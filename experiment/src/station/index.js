const request = require('../request')
const { provJS } = require('../prov')
const backup = require('../../stations.json')

const getStation = async stationId =>
  request(`https://api.tfl.lu/v1/StopPoint/${stationId}`, { json: true })

const getStationFromFile = stationId => {
  return backup.features.find(feature => {
    return feature.properties.id === stationId
  })
}

async function enrichStation ({data, metadata}, stationKey, stationId) {
  let resp
  let uptodateness = 1.00
  if (Math.random() > 0.15) {
    resp = await getStation(stationId)
  } else {
    resp = getStationFromFile(stationId)
    uptodateness = 0.30
  }
  const stationName = `dataproduct:station-${stationKey}`
  metadata.entity(stationName, [
    'datamarket:cost', '0.001',
    'rights:derivationRights', true,
    'rights:commercialUse', true,
    'dataquality:accuracy', 1.00,
    'dataquality:completeness', 1.00,
    'dataquality:uptodateness', uptodateness,
    'control:LawandJurisdiction', 'LUX',
    'prov:type', 'datamarket:OriginDataProduct',
    'prov:time', new Date().toISOString(),
  ])
  metadata.wasAttributedTo(stationName, 'dataprovider:TransportForLuxembourg')

  return {
    data: {
      ...data,
      [stationKey]: resp
    },
    metadata
  }
}

module.exports = enrichStation
