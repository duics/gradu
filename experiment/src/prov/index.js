const provJS = require('./provJS');

function doNothing () {
  return null
}

const createDummyProv = () => ({
  agent: doNothing,
  entity: doNothing,
  activity: doNothing,
  used: doNothing,
  wasDerivedFrom: doNothing,
  wasGeneratedBy: doNothing,
  wasAttributedTo: doNothing
})

const createProv = () => {
  var prov = provJS.document();

  // Prefix declarations
  prov.addNamespace('datamarket', 'N/A');
  prov.addNamespace('rights', 'N/A');
  prov.addNamespace('dataquality', 'N/A');
  prov.addNamespace('dataproduct', 'N/A');
  prov.addNamespace('dataprovider', 'N/A');
  prov.addNamespace('compliance', 'N/A');
  prov.addNamespace('control', 'N/A');
  prov.addNamespace('pricing', 'N/A');

  return prov;
}

module.exports = { createProv: createProv };
