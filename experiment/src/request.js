const request = require('request-promise')
const dummyRequest = () => {
  return {
    geometry: {
      coordinates: [0, 0]
    },
    distances: [
      [0, 1]
    ],
    durations: [
      [0, 1]
    ],
    response: {
      matrixEntry: [{summary: null}]
    }
  }
}
Object.setPrototypeOf(dummyRequest, {
  post: function () {
    return {
      distance: [
        [0, 1]
      ],
      time: [
        [0, 1]
      ]
    }
  }
})

module.exports = request
