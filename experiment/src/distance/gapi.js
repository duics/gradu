const GAPI_KEY = process.env.GAPI_KEY;
const request = require('../request');

const getCoordinates = ({ geometry }) =>
 `${geometry.coordinates[1]},${geometry.coordinates[0]}`;

const getDistanceGAPI = async ({data, metadata}, departureStation, destinationStation) => {
  const startTime = new Date()
  const departure = getCoordinates(departureStation);
  const destination = getCoordinates(destinationStation);

  const resp = await requestData(departure, destination);
  const endTime = new Date()
  metadata.agent('dataprovider:GoogleMaps', ['prov:type', 'datamarket:DataProvider'])
  metadata.activity('dataproduct:enrichDistanceMatrix', startTime, endTime, ['prov:type', 'datamarket:createHybridDataProduct']);
  metadata.entity(`dataproduct:distanceMatrix`, [
    'pricing:cost', '0.002',
    'pricing:currency', 'EUR',
    'pricing:model', 'perUse',
    'rights:derivationRights', true,
    'rights:commercialUse', true,
    'dataquality:accuracy', 0.90,
    'dataquality:completeness', 1.00,
    'dataquality:uptodateness', 1.00,
    'control:LawandJurisdiction', 'EU',
    'prov:type', 'datamarket:OriginDataProduct',
    'prov:time', new Date().toISOString(),
  ])
  metadata.wasAttributedTo('dataproduct:distanceMatrix', 'dataprovider:GoogleMaps')
  metadata.entity('dataproduct:departureWithDistanceMatrix', [
    'datamarket:cost', '0.004',
    'prov:type', 'datamarket:HybridDataProduct'
  ]);
  metadata.wasDerivedFrom('dataproduct:departureWithDistanceMatrix', 'dataproduct:departureWithCoords', ['prov:type', 'prov:PrimarySource']);
  metadata.used('dataproduct:enrichDistanceMatrix', 'dataproduct:departureWithCoords', startTime);
  metadata.wasGeneratedBy('dataproduct:departureWithDistanceMatrix', 'dataproduct:enrichDistanceMatrix', endTime);

  return {
    data: {
      ...data,
      distanceMatrix: resp
    },
    metadata
  };
}

const requestData = async (origin, destination) => {
  const host = 'https://maps.googleapis.com/maps/api/distancematrix/json';
  const params = `key=${GAPI_KEY}&origins=${origin}&destinations=${destination}`;
  const url = `${host}?${params}`;
  return request(url, {json: true});
}

module.exports = getDistanceGAPI
