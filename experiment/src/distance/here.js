const HERE_ID = process.env.HERE_ID;
const HERE_CODE = process.env.HERE_CODE;
const request = require('../request');

const getCoordinates = ({ geometry }) =>
 `${geometry.coordinates[1]},${geometry.coordinates[0]}`;

const getDistanceHERE = async ({data, metadata}, departureStation, destinationStation) => {
  const startTime = new Date()
  const departure = getCoordinates(departureStation);
  const destination = getCoordinates(destinationStation);

  const resp = await requestData(departure, destination);
  const matrixData = resp.response.matrixEntry[0].summary;
  const endTime = new Date()
  metadata.agent('dataprovider:HEREMaps', ['prov:type', 'datamarket:DataProvider'])
  metadata.activity('dataproduct:enrichDistanceMatrix', startTime, endTime, ['prov:type', 'datamarket:createHybridDataProduct']);
  metadata.entity(`dataproduct:distanceMatrix`, [
    'pricing:cost', '0.002',
    'pricing:currency', 'EUR',
    'pricing:model', 'perUse',
    'rights:derivationRights', true,
    'rights:commercialUse', true,
    'dataquality:accuracy', 0.95,
    'dataquality:completeness', 1.00,
    'dataquality:uptodateness', 1.00,
    'control:LawandJurisdiction', 'EU',
    'prov:type', 'datamarket:OriginDataProduct',
    'prov:time', new Date().toISOString(),
  ])
  metadata.wasAttributedTo('dataproduct:distanceMatrix', 'dataprovider:HEREMaps')
  metadata.entity('dataproduct:departureWithDistanceMatrix', [
    'datamarket:cost', '0.004',
    'prov:type', 'datamarket:HybridDataProduct'
  ]);
  metadata.wasDerivedFrom('dataproduct:departureWithDistanceMatrix', 'dataproduct:departureWithCoords', ['prov:type', 'prov:PrimarySource']);
  metadata.used('dataproduct:enrichDistanceMatrix', 'dataproduct:departureWithCoords', startTime);
  metadata.wasGeneratedBy('dataproduct:departureWithDistanceMatrix', 'dataproduct:enrichDistanceMatrix', endTime);

  return {
    data: {
      ...data,
      distanceMatrix: matrixData
    },
    metadata
  };
}

const requestData = async (origin, destination) => {
  const host = 'https://matrix.route.api.here.com/routing/7.2/calculatematrix.json';
  const params = `app_id=${HERE_ID}&app_code=${HERE_CODE}&start0=geo!${origin}&destination0=geo!${destination}&mode=fastest;car;traffic:disabled&summaryAttributes=distance,traveltime`;
  const url = `${host}?${params}`;
  return request(url, {json: true});
}

module.exports = getDistanceHERE
