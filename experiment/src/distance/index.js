const getDistanceGAPI = require('./gapi')
const getDistanceHERE = require('./here')
const getDistanceOpenRoute = require('./openRouteServices')
const getDistanceMapQuest = require('./mapquest')
const providers = [
  getDistanceGAPI,
  getDistanceHERE,
  getDistanceOpenRoute,
  getDistanceMapQuest,
]

async function enrichDistance(payload) {
  const { data, metadata } = payload
  const { departureStation, destinationStation } = data;
  const fun = providers[Math.floor(Math.random()*providers.length)]
  return await fun(payload, departureStation, destinationStation)
}

module.exports = enrichDistance;
