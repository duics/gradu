const OPENROUTE_KEY = process.env.OPENROUTE_KEY;
const request = require('../request');

const getCoordinates = ({ geometry }) =>
 `${geometry.coordinates[0]},${geometry.coordinates[1]}`;

const getDistanceOpenRoute = async ({data, metadata}, departureStation, destinationStation) => {
  const startTime = new Date()
  const departure = getCoordinates(departureStation);
  const destination = getCoordinates(destinationStation);
  const resp = await requestData(departure, destination);
  const matrixData = {
    distance: resp.distances[0][1],
    duration: resp.durations[0][1],
  };

  const endTime = new Date()
  metadata.agent('dataprovider:OpenRouteMaps', ['prov:type', 'datamarket:DataProvider'])
  metadata.activity('dataproduct:enrichDistanceMatrix', startTime, endTime, ['prov:type', 'datamarket:createHybridDataProduct']);
  metadata.entity(`dataproduct:distanceMatrix`, [
    'pricing:cost', '0.001',
    'pricing:currency', 'EUR',
    'pricing:model', 'perUse',
    'rights:derivationRights', true,
    'rights:commercialUse', true,
    'dataquality:accuracy', 0.90,
    'dataquality:completeness', 1.00,
    'dataquality:uptodateness', 1.00,
    'control:LawandJurisdiction', 'EU',
    'prov:type', 'datamarket:OriginDataProduct',
    'prov:time', new Date().toISOString(),
  ])
  metadata.wasAttributedTo('dataproduct:distanceMatrix', 'dataprovider:OpenRouteMaps')
  metadata.entity('dataproduct:departureWithDistanceMatrix', [
    'datamarket:cost', '0.003',
    'prov:type', 'datamarket:HybridDataProduct'
  ]);
  metadata.wasDerivedFrom('dataproduct:departureWithDistanceMatrix', 'dataproduct:departureWithCoords', ['prov:type', 'prov:PrimarySource']);
  metadata.used('dataproduct:enrichDistanceMatrix', 'dataproduct:departureWithCoords', startTime);
  metadata.wasGeneratedBy('dataproduct:departureWithDistanceMatrix', 'dataproduct:enrichDistanceMatrix', endTime);

  return {
    data: {
      ...data,
      distanceMatrix: matrixData
    },
    metadata
  };
}

const requestData = async (origin, destination) => {
  const host = 'https://api.openrouteservice.org/matrix';
  const params = `api_key=${OPENROUTE_KEY}&profile=driving-car&metrics=duration|distance&locations=${origin}|${destination}`;
  const url = `${host}?${params}`;
  return request(url, {json: true});
}

module.exports = getDistanceOpenRoute
