const MAPQUEST_KEY = process.env.MAPQUEST_KEY;
const request = require('../request');

const getCoordinates = ({ geometry }) =>
 `${geometry.coordinates[1]},${geometry.coordinates[0]}`;

const getDistanceMapQuest = async ({data, metadata}, departureStation, destinationStation) => {
  const startTime = new Date()
  const departure = getCoordinates(departureStation);
  const destination = getCoordinates(destinationStation);

  const resp = await requestData(departure, destination);
  const endTime = new Date()
  metadata.agent('dataprovider:MapQuest', ['prov:type', 'datamarket:DataProvider'])
  metadata.activity('dataproduct:enrichDistanceMatrix', startTime, endTime, ['prov:type', 'datamarket:createHybridDataProduct']);
  metadata.entity(`dataproduct:distanceMatrix`, [
    'pricing:cost', '0.002',
    'pricing:currency', 'EUR',
    'pricing:model', 'perUse',
    'rights:derivationRights', true,
    'rights:commercialUse', true,
    'dataquality:accuracy', 0.90,
    'dataquality:completeness', 1.00,
    'dataquality:uptodateness', 1.00,
    'control:LawandJurisdiction', 'EU',
    'prov:type', 'datamarket:OriginDataProduct',
    'prov:time', new Date().toISOString(),
  ])
  metadata.wasAttributedTo('dataproduct:distanceMatrix', 'dataprovider:MapQuest')
  metadata.entity('dataproduct:departureWithDistanceMatrix', [
    'datamarket:cost', '0.004',
    'prov:type', 'datamarket:HybridDataProduct'
  ]);
  metadata.wasDerivedFrom('dataproduct:departureWithDistanceMatrix', 'dataproduct:departureWithCoords', ['prov:type', 'prov:PrimarySource']);
  metadata.used('dataproduct:enrichDistanceMatrix', 'dataproduct:departureWithCoords', startTime);
  metadata.wasGeneratedBy('dataproduct:departureWithDistanceMatrix', 'dataproduct:enrichDistanceMatrix', endTime);

  return {
    data: {
      ...data,
      distanceMatrix: {
        distance: resp.distance[1],
        time: resp.time[1],
      }
    },
    metadata
  };
}

const requestData = async (origin, destination) => {
  const host = 'http://www.mapquestapi.com/directions/v2/routematrix';
  const params = `key=${MAPQUEST_KEY}`;
  const body = {
    locations: [origin, destination],
  }
  const url = `${host}?${params}`;
  return request.post(url, {json: true, body});
}

module.exports = getDistanceMapQuest
