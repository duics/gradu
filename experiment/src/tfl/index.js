const IO = require('socket.io-client');
const url = require('url');
const { createProv } = require('../prov')

class Client {
  constructor(urlString) {
    urlString = urlString || 'https://api.tfl.lu/v1';
    this.host = url.resolve(urlString, '/');

    var { pathname } = url.parse(urlString);
    this.path = pathname.replace(/\/$/, '') + '/stream';

    this.subscriptions = {};
    this.io = IO(this.host, {
      path: this.path
    });
    this.io.on('data', update => {
      if (!update.path || !this.subscriptions[update.path] || update.data.type !== 'new') {
        return;
      }

      const processedData = this.postprocess(update.data);
      this.subscriptions[update.path].forEach(callback => callback(processedData || {}));
    });
  }

  subscribe(path, callback) {
    if (!this.subscriptions[path]) {
      this.subscriptions[path] = [];
      this.io.emit('subscribe', path);
    }
    this.subscriptions[path].push(callback);
  }

  postprocess(payload) {
    const data = payload.data;
    const station_id = Object.keys(data)[0];
    const raw_departures = data[station_id];
    const departures_with_station =
      raw_departures.map((departure) => {
        departure.departureId = station_id;
        return {data: departure, metadata: departureProv()};
      });
    return departures_with_station;
  }
}

const departureProv = () => {
  const provDoc = createProv()
  provDoc.agent('dataprovider:TransportForLuxembourg', ['prov:type', 'datamarket:DataProvider'])
  provDoc.entity('dataproduct:departure', [
    'pricing:cost', '0.001',
    'pricing:currency', 'EUR',
    'pricing:model', 'perUse',
    'rights:derivationRights', true,
    'rights:commercialUse', false,
    'dataquality:accuracy', 1.00,
    'dataquality:completeness', 1.00,
    'dataquality:uptodateness', 1.00,
    'control:LawandJurisdiction', 'LUX',
    'prov:type', 'datamarket:OriginDataProduct',
    'prov:time', new Date().toISOString(),
  ]);
  provDoc.wasAttributedTo('dataproduct:departure', 'dataprovider:TransportForLuxembourg')
  return provDoc
}

module.exports = {Client, departureProv};
