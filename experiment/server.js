// const GAPI_KEY = process.env.GAPI_KEY
// const weather = require('./src/weather')
const signale = require('signale')
const pipeline = require('./pipeline')
const { Client } = require('./src/tfl')

let tflClient = new Client()

// Multiple departures can be returned in a single update, which have to be
// splitted
const feedToPipeline = (departures) => {
  departures.map(departure => {
    if (departure.data.type != "bus") {
      return
    }
    pipeline(departure)
  })
}

// Subscribe to departing buses and trains
tflClient.subscribe('/StopPoint/Departures', feedToPipeline)