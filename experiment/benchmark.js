const Benchmark = require('benchmark');
const math = require('mathjs')
const pipeline = require('./pipeline');
const { departureProv } = require('./src/tfl')

Benchmark.options.minSamples = 100
// Benchmark.options.minTime = -Infinity
// Benchmark.options.maxTime = -Infinity

const suite = new Benchmark.Suite()

// add tests
suite.add('pipeline', {
  'defer': true,
  'fn': function (deferred) {
    const dummyPayload = {
      data: {
        departureId: 300000081,
        destinationId: 300000081
      },
      metadata: departureProv()
    }
    pipeline(dummyPayload).then(() => {
      deferred.resolve()
    })
  }
})
// add listeners
.on('cycle', function(event) {
  const factor = 1000
  let { sample, deviation, mean } = event.target.stats
  console.log(String(event.target));
  // console.log(event.target.stats)
  const max = sample.reduce(function(a, b) {
      return Math.max(a, b);
  });
  const min = sample.reduce(function(a, b) {
      return Math.min(a, b);
  });
  console.log(event.target.stats)
  console.log(`Mean: ${mean*factor}`)
  console.log(`Max: ${max*factor}`)
  console.log(`Min: ${min*factor}`)
  const [lower,,upper] = quartileBounds(sample)
  console.log(`lq: ${lower*factor}`)
  console.log(`uq: ${upper*factor}`)
  console.log(`median: ${math.median(sample)*factor}`)
})
// run async
.run({ 'async': true, minSamples: 1000 });

function quartileBounds(_sample){
  // find the median as you did
  var _median = math.median(_sample)

  // split the data by the median
  var _firstHalf = _sample.filter(function(f){ return f < _median })
  var _secondHalf = _sample.filter(function(f){ return f >= _median })

  // find the medians for each split
  var _25percent = math.median(_firstHalf);
  var _75percent = math.median(_secondHalf);

  var _50percent = _median;
  var _100percent = math.max(_secondHalf);

  // this will be the upper bounds for each quartile
  return [_25percent, _50percent, _75percent, _100percent];
}