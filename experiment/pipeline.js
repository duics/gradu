const signale = require('signale')
const fs = require('fs')
const enrichStation = require('./src/station')
const enrichDistance = require('./src/distance')
let increment = 0

async function pipeline (payload) {
  // The departure data is first expanded with both departure and destination
  // station locations using coordinate data, forming a hybrid data product

  if (!payload.data.departureId) {
    signale.error('Departure without departureId')
    return
  }

  if (!payload.data.destinationId) {
    signale.error('Departure without destinationId')
    return
  }

  signale.start(`Departure event received from ${payload.data.departureId}...`);

  payload = await addStations(payload)

  signale.watch('Stations enriched');

  if (!payload.data.departureStation) {
    console.log('no geometry')
    console.log(payload.data.departureStation)
    // signale.error("Something broke down")
    return;
  }
  payload = await enrichDistance(payload)

  signale.watch('Distance matrix enriched');

  fs.writeFile(`/tmp/dataproduct-${increment++}`, payload, () => {
    signale.success("Dataproduct complete!")
  })
  // console.log(JSON.stringify(payload.data));
  // const provjson = payload.metadata.scope.getProvJSON();
  // console.log(JSON.stringify(provjson));
}

async function addStations (payload) {
  const {data, metadata} = payload;
  const startTime = new Date()

  metadata.agent('dataprovider:TransportForLuxembourg', [])
  payload = await enrichStation(payload, 'departureStation', data.departureId)
  payload = await enrichStation(payload, 'destinationStation', data.destinationId)

  const endTime = new Date()
  metadata.activity('dataproduct:enrichCoordinates', startTime, endTime, ['prov:type', 'datamarket:createHybridDataProduct']);
  metadata.entity('dataproduct:departureWithCoords', [
    'datamarket:cost', '0.002',
    'prov:type', 'datamarket:HybridDataProduct'
  ]);
  metadata.wasDerivedFrom('dataproduct:departureWithCoords', 'dataproduct:departure', ['prov:type', 'prov:PrimarySource']);
  metadata.wasDerivedFrom('dataproduct:departureWithCoords', 'dataproduct:station-departureStation');
  metadata.wasDerivedFrom('dataproduct:departureWithCoords', 'dataproduct:station-destinationStation');
  metadata.used('dataproduct:enrichCoordinates', 'dataproduct:departureWithCoords', startTime);
  metadata.used('dataproduct:enrichCoordinates', 'dataproduct:dataset1', endTime);
  metadata.used('dataproduct:enrichCoordinates', 'dataproduct:dataset2', endTime);
  metadata.wasGeneratedBy('dataproduct:departureWithCoords', 'dataproduct:enrichCoordinates', endTime);

  return payload
}

module.exports = pipeline