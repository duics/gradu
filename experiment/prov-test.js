'use strict';

var { createProv } = require('./src/prov');

var util = require('util')

function primer() {
  'use strict';

  doc = createProv()

  const now = new Date().toISOString()

  doc.agent('dataprovider:UniversityOfJyvaskyla', ['prov:type', 'datamarket:DataProvider'])
  // Entities
  doc.entity('dataproduct:exampleData1', [
    'prov:type', 'datamarket:OriginDataProduct',
    'prov:time', now,
    'pricing:cost', 0.001,
    'pricing:currency', 'EUR',
    'pricing:model', 'perUse',
    'control:LawandJurisdiction', 'FI',
  ]);
  doc.wasAttributedTo('dataproduct:exampleData1', 'dataprovider:UniversityOfJyvaskyla')
  doc.entity('dataproduct:exampleData2', [
    'prov:type', 'datamarket:OriginDataProduct',
    'prov:time', now,
    'pricing:cost', 0.001,
    'pricing:currency', 'EUR',
    'pricing:model', 'perUse',
    'control:LawandJurisdiction', 'FI',
  ]);
  doc.wasAttributedTo('dataproduct:exampleData2', 'dataprovider:UniversityOfJyvaskyla')

  doc.activity('dataproduct:combineExampleData', now, now, ['prov:type', 'datamarket:createHybridDataProduct']);
  doc.used('dataproduct:combineExampleData', 'dataproduct:exampleData1', now);
  doc.used('dataproduct:combineExampleData', 'dataproduct:exampleData2', now);

  doc.entity('dataproduct:exampleHybridData', [
    'pricing:cost', 0.002,
    'pricing:currency', 'EUR',
    'pricing:model', 'perUse',
    'prov:type', 'datamarket:HybridDataProduct'
  ]);
  const useTime = Date.now()
  // Usage and Generation
  doc.wasGeneratedBy('dataproduct:exampleHybridData', 'dataproduct:combineExampleData', Date.now());

  doc.wasDerivedFrom('dataproduct:exampleHybridData', 'dataproduct:exampleData1', ['prov:type', 'prov:PrimarySource']);
  doc.wasDerivedFrom('dataproduct:exampleHybridData', 'dataproduct:exampleData2');

  return doc.scope;
}

var doc = primer();
var provjson = doc.getProvJSON();

console.log(JSON.stringify(provjson));