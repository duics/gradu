\chapter{DATA MARKETPLACES}
\label{chap:marketplaces}

% TODO: Cites
Finding the appropriate data sources with the right types of data can sometimes
be challenging. Either the data is not available in the open, access to it is
hidden behind obscure interfaces, or it's simply not in a usable form. In the
early years of the web, there were even professionals whose sole task was to
search the Web for the desired information and return the results to the client
\parencite{Schomm2013}. As the technology evolved, so did the methods of
information exchange. Websites known as ``mashups'' that combine information
sources from multiple sites became popular, enabling users to combine prices for
products from different stores, or trip prices between different airlines
\parencite{Zhu2009}. Data marketplaces take this a step further by making
the data itself into a product and by providing a way to trade this information
on a single platform. These platforms typically operate in the internet and can
be structured in multiple different ways. \parencite{Schomm2013}

In this chapter, we will investigate the concept of Data marketplaces from
organizational perspective. The literature used in this chapter was mostly
discovered with search engines (\texttt{kirjasto.jyu.fi},
\texttt{scholar.google.com}) using relevant keywords (e.g. "Data
marketplace(s)"). Additional sources were found by following and
investigating papers that had cited the literature discovered with search
engines.

\section{Introduction to data marketplaces}
\label{sec:marketplace-intro}

\noindent
To understand data marketplaces, it should first be understood what is meant
with the basic economical terms behind the concept of data marketplaces.
\textit{Markets} are places where the interactions of buyers and sellers set
prices and quantities of goods and services. \textit{Marketplaces} are 
concrete locations that facilitate markets: Explicit places at an explicit
time, where market participants can prepare and execute transactions. By
facilitating the interactions between market participants, marketplaces
provide the fundamental infrastructure for trading. \parencite{Stahl2016}

In general, the market serves three key functions: Firstly, the market serves
as an \textit{institution}: A framework of rules that govern behavior for the
participants at the market. Institution assigns roles, expectations, and
protocols for trading on the medium. Second, markets provide a
\textit{pricing mechanism} for buyers and sellers, the condition of exchange,
which operates as an equalizer of supply and demand. If the demand rises
above supply, prices rise to compensate. Accordingly, a large surplus with
little demand would lead to prices decreasing. Finally, the market defines
the process of \textit{transactions}.

According to \textcite{Richter2002} (as cited by \textcite{Schomm2013}),
transactions can be broken down to four distinct phases (visualized in
\autoref{fig:transaction-phases}):

\begin{description}
  \item[Information phase]
    Market participant seeks information on the goods and create an intention of
    exchange with bids and offers.
  \item[Negotiation phase]
    Negotiate on the goods between buyer and seller. Set contract terms and
    price for the good, which forms a contract
  \item[Transaction phase]
    Fulfillment of contract, where the good is exchanged from seller to buyer
  \item[After-sales phase]
    After the contract has been fulfilled, the buyers satisfaction and
    commitment can be enhanced from the side of seller with customer support.
\end{description}

\begin{figure}[h]
  \centering
  \includegraphics[height=7cm,keepaspectratio]{kuviot/transaction_phases}
  \caption[Phases of a market transaction]{Phases of a market transaction according to \textcite{Schmid1998}}
  \label{fig:transaction-phases}
\end{figure}

With marketplaces also appearing in the Web, \textcite{Schmid1998} (as cited by
\textcite{Stahl2016}) defined the concept of electronic marketplaces to be the an
electronic medium that is based on the new digital communication and transaction
infrastructure. In comparison to traditional markets, electronic markets have
major advantages in terms of high accessibility, low entry barriers, ubiquity,
and lower transaction costs. However, ubiquity can also complicate the effort
needed to maintain an electronic market as rules, legislation and language
might vary between different areas of operation. \parencite{Stahl2016}

For a market to be considered at least partially electronic, at least one phase
needs to be done electronically. Some researchers also consider an
electronically performed information phase to be the absolute minimum for an
electronic marketplace as it enables participation to the market without the
conventional limits of location and time. \parencite{Schomm2013}

Data marketplaces extend on the concept of electronic marketplaces by
providing a marketplace where the commodity is data. At the most general
level, the three main categories of data marketplace users are \textbf{data
consumers} (buyers) who use the data to their needs, \textbf{data providers}
(sellers) who give an access to data, and \textbf{data marketplace owners}
who own the infrastructure on which the trading occurs
\parencite{Muschalle2012}. Data marketplaces provide a method of accessing
variety of information through a single platform that makes it easier for
participants trade data. To access the data, data marketplaces must have
infrastructure to enable the browsing, uploading, and downloading the data
while also facilitating transactions (i.e. buying and selling) between the
participants of the platform. The origin of the data must also be tracked: It
must be clear from where the data is public or property of a member who is
exchanging data on the platform. \parencite{Stahl2016}

% TODO: How does commodization affect value?
Data is an abstract digitized good which makes estimating it's value difficult.
One way to approach the problem of how to valuate data is through commoditization,
a process of product standardization. The level of commoditization dictates how
much a product diverges from other similar products. In the case of data, a
highly commoditized data would be in a standardized format and it could be used
with the same tools and applications as similar data from an alternative data
provider. Machine-readability of data is one of the main indicators of
commoditization. Uncommoditized data, however, would be highly different from
other data sets without a specific standardized way to access it. The more
commoditized the data product is, the more perfect the market will be in terms
of competition and freedom. There are cases where uncommditized data might be a
more appropriate choice as perfect markets might not be the goal for those
services: Wikipedia is an example of an uncommoditized data market where data is
shared for altruistic purposes so that everyone can benefit from it. The sharing
and browsing of data occurs through the browser, but is not easily read by
machine due to its unstructured form.
\parencite{Stahl2015}

Data products on the data marketplace are sold by data providers. In order to
fulfill the definition of a data provider, at least one of the four
prerequisites that must be true \textcite{Stahl2015}:

\begin{itemize}
\item The primary business model of the provider must be providing data.
\item Provider owns or makes a platform available for others where data can be
browsed, uploaded or downloaded in a machine-readable format. The data on the
platform must be hosted by the provider and the origin of data must be detailed.
\item Provider offers or sells proprietary data hosted by themselves. Similar to
previous item, the origin of data must be traceable and transparent. In
addition, data that has been processed must include the original sources and
details on the method for achieving the result.
\item Providers who offer data processing services in the form of data analysis
tools must be web-based and provide storable data as their main offering.
\end{itemize}

Furthermore, government agencies and providers who host their data for free are
typically excluded from this definition. This is due to them offering the data
as a side effect of their other operations, and they are typically not
interested commoditizing data or finding new business models to transform their
data into a business. One example of this is the Open Government movement that
seeks to provide access to data from governments to increase transparency and
citizen participation. \parencite{Stahl2015}

Pricing on the data marketplaces varies largely based on the overall market
situation \parencite{Muschalle2012}. According to the survey by
\textcite{Muschalle2012}, there exists multiple different types of pricing models that
tie into the business model of the data provider. \textbf{Usage based pricing}
model is used to put a price for each unit (be at data, bandwidth, or time) that
the customer uses. This might include pricing the data by the amount of times
that the customer queries the data service, or time and effort that it takes for
data provider to produce the data customer desires. Pricing per usage does,
however, has a weakness of losing its attractiveness as the marginal costs to
produce the data approaches zero -- at the time of the
research there didn't exist a single data provider who would offer pure per-usage
pricing with queries. \textbf{Package pricing} is used to provide fixed amount
of service for a single payment. This might limit the usage of data in a certain
time window (e.g. 1000 queries in a day) or have some other restrictions for the
usage of data. Package pricing is a popular choice and is being offered by many
data providers. \textbf{Flat fee tariffs} are used to give a full access to the
offering for a specified amount of time. Although the pricing model is simple and gives
guarantees of continued income for the provider, the pricing model is not very
flexible from the perspective of consumer as it requires planning into future.
To address the concerns of consumers, the provider might offer more flexible
short-term contracts. \textbf{Two-part tariffs} extend on the previous model: In
addition to the fixed fee, the consumers also pay a per-use fee for each
consumed unit. The pricing model can, for example, be used from the side of the
provider in a way that the fixed part covers the running costs, while the
per-use prices bring in the profit. Some software licensing models also use the
model: consumers might pay a fixed fee for the base product, and an additional
fee based on the amount of users that use the product. \textbf{Freemium} model
provides two-tier service the consumers by offering the basic service for free,
and adding a price to premium features such as data integration. The pricing
model of premium features may be any of the formerly mentioned models. Lastly,
one of the pricing approaches is also not asking a price at all. Data can be
given for free, but providers often use it as a way to attract customer towards
their other offerings that do have a price.

Aside from the pricing models discovered in the survey by \textcite{Muschalle2012},
there also exists alternative pricing models in literature. \textcite{Koutris2015} presents a framework for
query-based pricing that can be used to automatically assign a price to more
complex and custom ad-hoc queries, allowing data providers to extend their
offering beyond a limited set of views on the data. \textcite{Tang2013} created a
generic pricing model that builds on the ideas of \textcite{Koutris2015} and assigns
a value to each tuple (or a row) on the database, with each query priced based on
the set of results included in the query. In addition, another paper by \textcite{Tang2014}
details an algorithm based on the idea of setting the price based on the
completeness of the requested data -- the consumer would pay less if they asked
for a limited sample of the whole query. 

\section{Categories of products and users on data marketplaces}

\noindent
Currently existing data marketplaces provide a base for investigating the
taxonomy of data products and data users. Surveys done by
\textcite{Muschalle2012}, \textcite{Schomm2013}, \textcite{Stahl2014}, and
\textcite{Stahl2015} investigated the various data markets and formed
categorizations based on the findings. The first category is the types of
data offerings that data providers provide, listed in \autoref{tbl:dataproducts}. The
data offerings are not mutually exclusive allowing a single provider to have
multiple offerings from different categories of data products.

\begin{table}[H]
\caption[Data products]{Different data offerings according to \textcite{Schomm2013}}
\vspace{2 mm}
\begin{tabularx}{\textwidth}{|p{3cm}|X|}
  \hline
  \textbf{Data product} & \textbf{Description} \\ \hline
  Web crawler & 
  Web crawlers are services that seek and gather data from websites
  automatically based on pre-specified rules. Web crawlers have two
  categories: Focused crawlers that are typically bound to one specific area
  of domain (e.g. blogs or social media) and customizable crawlers that can
  be configured by the customer to gather data from any arbitrary web source.
  \\ \hline
  Search engine & 
  Search engines are services that provide an interface similar to web search
  engines such as Google. Search engines query the data sources they are
  attached to based on a set of keywords that are given by the customer.
  \\ \hline
  Raw data & 
  Raw data is data in an unprocessed form typically in the format of lists or
  tables.
  \\ \hline
  Complex data & 
  Complex data is data that has been processed or refined in some manner.
  \\ \hline
  Data matching & 
  Data matching is a service for correcting or verifying data that the
  customer already has. Instead of providing complete data sets, data
  matching can be used to cross-reference data of the customer to their
  internal data set to discover any discrepancies. One example use case could
  be checking validity of customer shipping addresses by comparing it to
  various address databases.
  \\ \hline
  Data enrichment & 
  Data enrichment provides different methods for increasing the value of data
  itself by altering it (e.g. adding additional information to the data).
  Data enrichment has three subcategories: Firstly, data tagging adds
  additional information to the input data in the form of tags, which is
  typically used to find details about unstructured text data. Second,
  sentiment analysis can be used to get data on how people feel about
  products, services and other matters of interests to the customer of
  sentiment analysis provider. Lastly, data analysis can be used investigate
  and enrich input data with insights like future trends and forecasts.
  \\ \hline
  Data marketplace & 
  Data marketplaces are also considered a data offering as they facilitate a
  service where customers can buy, sell and trade data.
  \\ \hline
\end{tabularx}
\label{tbl:dataproducts}
\end{table}

Similarly, there exists multiple different users for the data products listed
in \autoref{tbl:dataproducts}. Some of the users are more technical, using
the platforms programmatically through APIs, while others abstract the
details behind user interfaces and use data products in order to discover
insights that interest them. In a set of interviews over organizations that
use data markets, \textcite{Muschalle2012} discovered seven different groups
of data marketplace users. These users are listed in \autoref{tbl:datausers}.

\begin{longtable}{| p{.20\textwidth} | p{.75\textwidth} |}
  \caption[Data marketplace users]{Different users of data marketplaces according to \textcite{Muschalle2012}}
  \label{tbl:datausers}
  \endfirsthead
  \endhead
  \hline
  \textbf{Data user} & \textbf{Description} \\ \hline
  Analysts & 
  Analysts try to discover trends and insights from data. This leads them to
  utilize multiple sources of data ranging from public data sets on the web,
  search engines for discovery, private internal data from enterprises, and data
  acquired from other services such as data markets. As analysts seek insight by
  combining data with exploratory techniques, they also create a demand for data
  relevant to their needs. Members of this group include business analysts,
  marketing managers, sales executives, and other roles that benefit from
  analytical techniques.
  \\ \hline
  Application vendors & 
  Application vendors develop applications that make the use of data from
  data markets easier based on the requirements from analysts. Applications
  might provide easy-to-use interfaces that lower the barrier of access and
  allow a broader group of users to take advantage of data from a data
  market. Alternatively, they might simply provide procedures to query and
  aggregate data so that it can be used elsewhere. Some example applications
  include business analytics applications, customer relationship management
  applications, and enterprise resource planning systems.
  \\ \hline
  Developers of data associated algorithms & 
  To get the data in a form that is usable by analysts and application vendors, it
  might have to be transformed or otherwise processed into a desired format.
  Developers of data associated algorithms create pipelines for various tasks,
  such as data mining, cleaning, matching, and other purposes. These pipelines
  could also be integrated to the data marketplace as custom functionality that
  could be bought similarly to the data on the platform.
  \\ \hline
  Data providers & 
  Data providers store, sell and advertise data. In addition, some data providers
  might also have data integration offerings similar to the developers of data
  associated algorithms. There exists commercial and non-commercial providers:
  Non-commercial providers range from web search engines such as Google and Bing,
  to free-to-access web archives. Commercial providers include companies like
  Reuters and Bloomberg who sell financial and geographical data.
  \\ \hline
  Consultants & 
  Consultants act as support for organizations that require assistance with
  tasks related to the selection, integration, and evaluation of data for
  analysts and product development.
  \\ \hline
  Licensing and certification entities & 
  Licensing and certification entities are sometimes used by the data
  providers to ensure that data sets, applications and algorithms on the
  platform are appropriately licensed, or conform to a certain certification.
  This is also used to assist the customer in choosing data related products.
  \\ \hline
  Owner of the data marketplace & 
  The entity that owns the market. Owner of the data market is responsible
  for the technical, ethical, legal, and economical challenges that rise from
  the users of the platform, technical details of the platform, and legal
  aspects for areas on which the platform operates on.
  \\ \hline
\end{longtable}

\section{Data marketplace structures}

\noindent
The structure of marketplaces affects how people interact on the platform. In
this thesis, we describe the overall market structures based on the findings
of \textcite{Muschalle2012}, and a later classification for more specific
marketplaces that was founded on electronic marketplace research by
\textcite{Stahl2016}.

\textcite{Muschalle2012} divides the market into three categories: Monopolies,
oligopolies and strong competition. Monopolies in data markets imply
situations where a data provider has no competition. As there exists no alternative
data products, the provider can set prices freely to maximize their profits. This
allows the provider to do selective pricing, otherwise known as \textit{price
discrimination}, to set different prices for different types of customers to
optimize profits earned from each customer. Oligopolies are the next step from
monopolies. When one or more competitors exist, monopolistic pricing no longer
works as the customers would simply move to the competitors offerings. In a
competition for market share, providers might adjust pricing competitively
(``races to the bottom'') or try to compromise with each other to improve
profits. A strong level of competition will shift the market towards the ideal
of perfect markets as prices of offerings will approach their marginal cost.
Transparency between competing providers is improved as consumers of data will
want to compare offerings between the different available providers. Providers
themselves will no longer have the market-power to set prices (as nobody would
buy their product) and must abide by the trends set by the market. However, this
market situation carries risks for the data provider: Even if the gross margins
from trading data would be profitable, the overall costs of the provider might not
be covered by margins that are too thin. To counter this, a provider would have to
either add unique value propositions to their products to stand out from
competition and justify larger margins, or alternative cut overall costs to
avoid loss of profits.

\textcite{Stahl2016} created a classification framework for data marketplaces
that identifies six data marketplace Business Models on the scale of
orientation. The classification is used to indicate how freely users of the
marketplace are allowed to trade with each other on the platform;
market-oriented structures allow greater freedom of interaction, while
hierarchical structures restrict users of the platform to predefined
interactions. In addition, \textcite{Stahl2016} identified three ownership
structures that can affect the neutrality of the platform: Privately owned,
consortia-based, and independent. Private and consortia-based platforms might
have vested interests on the platform that can lead to bias towards the
owners themselves. Independent platforms, however, are run without
connections to providers or consumers which leads to a more even market
situation.

The \autoref{fig:marketplace-categories} illustrates potential ways to
structure a data marketplace based on the ownership model. Starting from the
hierarchical private ownership model, the structures in this category are
highly restrictive one-to-many or many-to-one relations, limiting the
interactions to either procuring or selling data between third parties and the
data provider (or the data buyer) who also owns the platform. Consortium-based
marketplaces, however, offer more freedom internally with many-to-many
relations between both the owner-providers of the platform and the third
parties. Even so, consortium-based platforms are typically collaborative
efforts by multiple companies and are typically closed by nature and
inaccessible by the public. \textcite{Stahl2016} also describes many-to-many
platforms where the owners participate by trading and selling their own data to
be in the same category as consortia-based marketplaces. Although such a
marketplace would not be a consortia-based, it would still behave like one due
to the bias towards the owners. Finally, the independent data marketplaces
operate closer to the principles of free markets by having little to no
restrictions for entry and simply acting as a mediators between the consumers
and providers. It should be noted that, according to \textcite{Stahl2016},
there exists little empirical research on data marketplaces aside from few
surveys. However, the model provides a base for observing new marketplaces that
emerge from new applications made possible by technologies such as cloud
computing.

\begin{figure}[h]
  \includegraphics[width=\textwidth,keepaspectratio]{kuviot/marketplace-categories}
  \caption[Hierarchy of marketplace structures]{Hierarchy of marketplace structures \parencite{Stahl2016}}
  \label{fig:marketplace-categories}
\end{figure}


\section{Requirements of multilateral data marketplaces}

\noindent
The requirements of data marketplaces can be summarized with findings of
\textcite{Koutroumpis2017}. As the focus of the thesis is on multilateral
marketplaces, we will not discuss requirements for other categories of data
marketplaces. For the general structure of the marketplace
\textcite{Koutroumpis2017} identified five key requirements.

\begin{enumerate}
    \item \textit{"Thickness" or the liquidity of market} indicates the
    number of participants on the data marketplace -- or a strong enough
    networking effect -- to ensure that there is enough diversity in the
    offerings and channels of trade. Without a sufficient amount of market
    participants, the market is unable to reach a critical mass that is
    required for it to grow in a meaningful manner.
    \item \textit{Performance and efficiency} are required to
    ensure small enough latencies in fulfilling transactions, and to provide
    enough througput so that the transactions will not slow down as the
    amount of participants rises. Technological choices and implementation
    details of the platform are used to address this requirement. Some
    approaches to performance and efficiency are discussed in
    \autoref{chap:data-as-a-service}.
    \item \textit{Perception of safeness} affects the degree of trust between
    market participants. The marketplace should have controls to provide
    sufficient deterrence for bad actors. Without necessary measures, it might be possible
    to manipulate the market or otherwise take an advantage over other
    participants in ways that reduces trust on the marketplace.
    \item \textit{Provenance of information} should be provided by the data
    marketplace. The origin, quality and other attributes of data
    should be made available to the buyer in order to prevent information asymmetry
    where the seller of data will know the details and quality of the data
    better than the buyer. Provenance is investigated with more detail in
    \autoref{chap:provenance}.
    \item \textit{Conforming to social and legal restrictions} affects the
    attraction of the data marketplace from the perspective of the market
    participants. Trading information with privacy implications, for
    instance, is a conflicting topic that has many societal and regulative
    barriers.
\end{enumerate}

The general requirements are fundamental for a data marketplace to stay
healthy and maintain growth. To facilitate the transactions on the platform
itself, the data marketplace must provide the two functional capabilities.
Firstly, a data marketplace must be capable of \textit{matching buyers with
sellers}, requiring either a manual mechanism that allows browsing, buying or
selling data on-demand, or alternatively an algorithmic matching mechanism
that will automatically connect sellers with buyers. Optionally, a
functionality of \textit{excludability} can be used to prevent undesirable
trades. Second, a data marketplace must be able to \textit{support provenance
with metadata} in order to protect data and enable data providers to control
their data assets while still allowing for innovative reuses of data.

\section{Data contracts}
\label{sec:data-contracts}

\noindent
One method of supporting the instititution of a data marketplace is through
data contracts, providing a framework for marketplace rules and pricing with
metadata. Data contracts are a type of service contracts that provide
information and guarantees on the nature of the data. The concept is
otherwise known as \textit{data agreements}, and the terms are used
interchangeably \parencite{Truong2011}. Current research on data contracts is
scarce \parencite{Koutroumpis2017} with the exception of
\textcite{Truong2009} applying the concept of data contracts in the context
of data marketplaces, and then following it with research on different models
of data contracts \parencite{Truong2011,Truong2012}. Currently most of the
data marketplaces use human-readable data agreements that have limitations in
terms of automation and usage in hybrid data products, to which data
contracts provide an alternative solution \parencite{Truong2011}.

\textcite{Truong2012} analyzed properties that are relevant for data
contracts in data marketplaces, leading to the formulation of five data
contract terms.

\begin{itemize}
    \item \textit{Data rights} declares the rights that the data provider
    grants to the consumer of that specific data. This includes the rights of
    \textit{Derivation} -- allowing modifications to the data asset that lead
    to a creation of a ``derivative work'', \textit{Collection} -- permitting
    the consumer of data to include the specific data set as a part of a
    collection of independent data sets, \textit{Reproduction} -- giving a
    right to create temporary or permanent reproductions of the data set,
    \textit{Attribution} -- how the original provider of data set is
    attributed for the use of data, and finally \textit{Noncommercial use} --
    whether the right to use data in non-commercial or commercial use is
    either denied or allowed.
    \item \textit{Quality of data} metrics may be included in the contract,
    including specifications such as completeness, reliability, accuracy,
    consistency and interpretability. The metrics should be based on common
    agreements that are established on the specific domain of the data set.
    \item \textit{Regulatory compliance} provides a list of regulations with
    a set of specifications that describes how the data complies specific
    regulations. As an example, handling personally identifiable information
    necessitates strict security measures due to compliance regulations.
    \item \textit{Pricing model} defines the method of pricing and the cost
    that the user of data must pay to the data provider.
    \item \textit{Control and relationship} provides information on
    contractual obligations, such as warranty, indemnity, liability, and
    jurisdiction.
\end{itemize}

These concepts were materialized in a data contract metadata model
presented in \autoref{tbl:datacontract-terms}.

\begin{table}[H]
\caption[Data contract metadata terms]{Data contract terms and values in the metadata model by \cite{Truong2012}}
% \vspace{2 mm}
{\renewcommand{\arraystretch}{1.5}%
\begin{tabularx}{\textwidth}{>{\hsize=.40\hsize}X|>{\hsize=.7\hsize}X|X}
\hline
Category & Term representation & Examples \\
\hline \hline
Data rights & \textit{termName=\{val\textsubscript{1}, val\textsubscript{2}, \dots , val\textsubscript{n}\}} & \textit{termName}\texttt{=\{Derivation, Collection, Reproduction, Attribution, Noncommercialuse\}}, \textit{val\textsubscript{i}}\texttt{=\{Undefined, Null, Allowed, Required, True, False\}} \\
\hline
Quality of Data & \textit{val\textsubscript{1} $\leq$ termName $\leq$ val\textsubscript{u}} & \hbox{\textit{val\textsubscript{l}, val\textsubscript{u}} $\in [0, 1]$} \textit{termName}\texttt{=\{Accuracy, Completeness,
Uptodateness\}} \\
\hline
Regulatory compliance & \textit{termName=\{val\textsubscript{1}, val\textsubscript{2}, \dots , val\textsubscript{n}\}} & e.g. \hbox{\textit{termName}=}\texttt{\{PrivacyCompliance\}} \hbox{\textit{termValue}=}\texttt{\{Sarbanes-Oxley (SOX) Act\}} \\
\hline
Pricing model & \textit{termName=(cost=val\textsubscript{1}, usagetime=tal\textsubscript{2}, maximumusage=tal\textsubscript{3}} & e.g. \textit{termName}=\texttt{\{MonthlyPayment\}}, val\textsubscript{1} $\in {\rm I\!R}$ (e.g. \texttt{cost=50\texteuro}), \textit{val\textsubscript{2}}=\{(\textit{end\textsubscript{t}} - \textit{start\textsubscript{t}}) or \texttt{UNLIMITED}\} where \textit{start\textsubscript{t}}, \textit{end\textsubscript{t}} $\in$ \textit{datetime} \\
\hline
Control \& Relationship & \textit{termName=val} & Any key/value string e.g. \textit{termName}\texttt{=\{Liability, LawandJurisdiction\}}, \textit{val}=\texttt{\{US, Austria\}} \\
\end{tabularx}}
\label{tbl:datacontract-terms}
\end{table}

\section{Summary}

\noindent
In this chapter we introduced the basics of markets. A market provide rules,
roles, protocols and pricing mechanisms for market transactions
\parencite{Stahl2016}. A market transaction has four phases: First the user
of the market seeks information on the goods, second the price is
negotiated between the buyer and seller, third the transaction is fulfilled
and the good is transferred from seller to buyer, and fourth customer
support might be provided by the seller after the transaction.
\parencite{Schomm2013}

Data marketplaces are electronic marketplaces that operate in the Web, providing
infrastructure for trading, browsing, uploading and downloading data
\parencite{Stahl2016}. Data is an intangible digitized good that makes
estimating its value challenging. However, the level of commodization, or the
machine readability of the data can help determining its value. Data products on
data marketplaces can be priced in multiple different ways, including
\textit{usage based pricing}, \textit{package pricing}, \textit{flat fee
  tariffs}, \textit{two-part tariffs}, and \textit{freemium models}.

A data marketplace has three general categories of users: \textit{Data
  consumers}, \textit{data providers}, and \textit{data marketplace owners}
\parencite{Muschalle2012}. Data providers may have multiple different data
product offerings (listed in \autoref{tbl:dataproducts}) and data marketplaces
have multiple different subcategories of users (seen in
\autoref{tbl:datausers}).

Data marketplaces can be structured in different ways, affecting the way in
which users of the data marketplace can interact with each other. The
structure of the data marketplace depends on the business model of the data
marketplace. Hierarchical data marketplaces have more strictly defined
interactions, while market-oriented data marketplaces place little to no
restrictions for user interactions. \parencite{Stahl2016}

The most general requirements for a successful multilateral data marketplace
are \textit{market liquidity}, \textit{performance and efficiency},
\textit{perception of safeness}, \textit{provenance of information}, and
\textit{conforming to social and legal restrictions}
\parencite{Koutroumpis2017}. The requirement of performance and efficiency is
investigated through data-as-a-Service concepts in
\autoref{chap:data-as-a-service} and provenance of information is further
discussed in \autoref{chap:provenance}. Lastly, data contracts provide a
framework for data marketplaces that can be used to provide relevant
information and guarantees about data products. \textcite{Truong2012}
identified five necessary data contract terms for data marketplaces:
\textit{Data rights}, \textit{quality of data}, \textit{regulatory
compliance}, \textit{pricing model}, and \textit{control and relationship}.