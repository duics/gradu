\chapter{INTRODUCTION}
\label{chap:introduction}

Data from different sources can have inter-dependencies that can be used to
discover correlations and other surprising insights. Air pollution metrics
require combining data from traffic, weather conditions, and industrial
emissions. Similarly a restaurant recommendation algorithm would also need
data from multiple sources: restaurant locations, customer reviews and user
preferences \parencite{Du2016}. However, an organization might not always
possess the required data assets or know-how to implement such algorithms
themselves. Data marketplaces address this issue by providing a platform
where organizations and individuals can trade data. Trading data can create
synergetic benefits for all participants that operate on the platform while
also enabling new innovative business models \parencite{Schomm2013,
Muschalle2012}. Sharing data assets could also highly enhance analytical
capabilities of organizations \parencite{Arafati2014}. The more varied and
vast the data assets, the better chance there is to find interesting
correlations that could lead to creation of new value. To mention a few
examples, industrial IoT data from infrastructure companies could be used by
a maintenance company to better understand when and where to dispatch workers
using predictive analytics. Car companies could share diagnostic data from
vehicles to accelerate development on self-driving cars. Finally, somewhat
controversially, advertising firms can combine data from multiple sources to
create more accurate advertising profiles on consumers.

There are multiple approaches to trading data between organizations. At
simplest, organizations could expose ports on their databases or simply
transmit the data for a single, non-generalizable purpose. Although such
approach of \textit{ad hoc} data trading is viable for the most basic use
cases, the approach becomes unsustainable at scale. Additionally, such
approach provides no solid method of tracking the origin and provenance of
data and other metadata related to it. Data marketplaces provide a mechanism
for trading data between organizations allowing data to be be repurposed for
new purposes with minimal use of management and development resources. The
difference between the two approaches is visualized in
\autoref{fig:datasharing}. When executed correctly, a data trading platform
can lower costs of data management \parencite{Arafati2014}. By utilizing
capabilities provided by cloud computing, the platform can scale freely to
save resources when there is little load on the system, and scale up when
there is higher demand. Similarly, cloud services provide high-capacity
storage as a service and application service provisioning that further
simplify the management of IT infrastructure \parencite{Stahl2016}.
Furthermore, sharing a data marketplace platform with other organizations
reduces the need for other organizations to redundantly create similar
infrastructures.
 

\begin{figure}[ht!]
  \begin{subfigure}[b]{0.5\textwidth}
    \smartdiagramset{
    uniform color list=gray!20 for 5 items,
    uniform arrow color=true,
    arrow color=transparent,
    module shape=circle, 
    module minimum width=0.5cm, 
    module minimum height=0.5cm, 
    text width=0.75cm,
    border color=white,
    circular distance=2.5cm,
    additions={
    additional arrow line width=2pt,
    additional arrow tip=to,
    additional arrow color=gray!90!black,
    additional item offset=3em,
    additional item height=0em,
    additional connections disabled=false,
    }
    }
    \tikzset{module/.append style={top color=\col,bottom color=\col}}
    \tikzset{every shadow/.style={fill=none,shadow xshift=0pt,shadow yshift=0pt}}
    \smartdiagramadd[circular diagram]{a,b,c,d,e}{}
    \smartdiagramconnect{to-to}{module1/module3}
    \smartdiagramconnect{-to}{module1/module2}
    \smartdiagramconnect{-to}{module2/module3}
    \smartdiagramconnect{to-to}{module2/module5}
    \smartdiagramconnect{to-to}{module3/module5}
    \smartdiagramconnect{to-to}{module3/module4}
    \smartdiagramconnect{-to}{module5/module1}
    \smartdiagramconnect{to-to}{module5/module4}
    \caption{Ad hoc data sharing}
  \end{subfigure}
  \begin{subfigure}[b]{0.5\textwidth}
    \smartdiagramset{
      uniform color list=gray!40 for 5 items,
      arrow color=gray!80!black,
      uniform connection color=true,
      distance planet-satellite=3cm,
      distance text center bubble=0cm,
      planet size=1.5cm,
      planet text width=1.75cm,
      satellite size=1cm,
      satellite text width=0.75cm,
      planet font= \normalfont,
      /tikz/connection planet satellite/.append style={<->}
    } 
    \smartdiagramadd[constellation diagram]{Platform, a, b, c, d, e}{}
    \caption{Data sharing on a platform}
  \end{subfigure}
\caption[Ad hoc vs. centralized]{Ad hoc data trading in comparison to trading
facilitated by a centralized platform. Nodes (e.g. a, b, c) represent
organizations, and arrows represent interactions}
\label{fig:datasharing}
\end{figure}

Managing a data marketplace platform has multiple and diverse challenges
\parencite{Koutroumpis2017}. In this thesis, the focus is on providing a
metadata model for hybrid data products. A metadata model provides a data
structure that contains information about the data itself. When multiple data
products are combined on a marketplace, a \textbf{hybrid data product} is
created. To track the provenance of data, metadata related to the original
data sources must somehow be included in the derivative data set.

The issues with tracking provenance of hybrid data products is highlighted by
\textcite{Koutroumpis2017}. In a situation where there are multiple data
vendors with multiple different licences, all derivative hybrid data products
must comply with the terms of every data product from which the hybrid data
product consists of. Assume a scenario with following data
providers: \textbf{Data provider A} offers a free and open data product,
\textbf{data provider B} provides a proprietary data product with contractual
term that requires royalties from derivate data products, \textbf{data
provider C} provides hybrid data product that combines data from \textbf{A}
and \textbf{B}, and lastly \textbf{data provider D} provides a hybrid data
product that uses data from \textbf{C} in addition to \textbf{D}'s own
proprietary data. Both \textbf{C} and \textbf{D} have to be sure that their
hybrid data products are compliant with the contractual terms of both
\textbf{A} and \textbf{B} (In addition to \textbf{C} in the case of
\textbf{D}). Lastly, all data product sales from \textbf{C} and
\textbf{D} require part of the cost being transferred to \textbf{B} in the
form of royalties.

There exists many concepts in literature that relate to the data
marketplaces: A \textbf{Data marketplace} is a platform where data can be
sold, bought or traded \parencite{Muschalle2012}. A \textbf{Data as a Service
(DaaS)} is a Cloud Computing service that provides data. The data is most
typically accessed via internet through a set of endpoints that allow various
types of operations on the data, such as querying and filtering. A set of
these endpoints form an Application Programming Interface (API) that can be
used by clients to build new types of services and applications, including
higher level platforms for trading data, such as data marketplaces
\parencite{Vu2012,Muschalle2012}. \textbf{Stream data} is data that arrives
gradually, typically in the form of events, and is potentially infinite
\parencite{Kleppmann2017}. For instance, smart meters send the electricity
usage readings of customers daily or even hourly, forming a data stream for
each customer. An \textbf{Event} is a data record that contains the details
of something that happened at some point of time \parencite{Kleppmann2017}.
An event could be a single reading from a smart meter, an overheating warning
from a power system, or even a notification that lights have been flicked on
in. \textbf{Stream processing} is an approach where data is processed as it
arrives with minimal latency, as opposed to gathering the data in arbitrarily
sized batches and processing them in bulk \parencite{Kleppmann2017}. Stream
processing enables reacting to data faster in comparison to batch methods,
enabling use cases that require low reaction times, such as fraud detection
\parencite{Stonebraker2005}. \textbf{Metadata}, at it's most general form, is
data about data. As the definition is very broad, \textcite{Deelman2010}
defines metadata to be ``structured data about an object that supports
functions associated with the designated object''. \textbf{Provenance} is a
record of events that can be used to determine the history of an object of
interest \parencite{Moreau2011}.

\section{Research problem, research questions, and limitations}

\noindent
The research problem was inspired by issues that were encountered in a certain
local company. Collaborating with other organizations and finding ways to
share and reuse data had an incentive problem: How to motivate other
organizations to share their data? This problem could largely be addressed
with a platform that provides a way for organizations to share data in
exchange for monetary compensation, or in other words, a data marketplace.

One specific use case of such data marketplace is the enrichment of
information with data from different sources. If organization A has a dataset
that could be combined with a dataset from organization B, how would a data
marketplace ensure that both organizations are fairly compensated when the
data is bought? Additionally, how would it work with industrial IoT devices
that provide data in the form of real-time streams, generating new data every
second?

In order to fairly compensate all parties on a data marketplace, the origin
of the data and the conditions of use must be known. Solving provenance is an
open research problem in the field of data marketplaces
\parencite{Koutroumpis2017}. The main research question of the thesis is:
\textit{``How to track provenience of hybrid data products on a multilateral
data marketplace?''}. The question lead into investigating the concept of
\textbf{data marketplaces} that are concerned with the business models and
organizational aspects of such platforms, the concept of \textbf{Data as a
Service} which provides perspective to technical and architectural concepts
of data marketplaces, and finally \textbf{provenience} that introduces the
challenges and approaches to tracking origin of data. Existing literature is
used as a base for a metadata model that can be used to track provenience of
hybrid data products and other metadata relevant to data marketplaces.

\section{Research method}

\noindent
The thesis follows Design Science Research Model by \textcite{Peffers2007}. The
resulting artifact from the thesis is an instantiation of the metadata model,
which is evaluated using a prototype that demonstrates the use of artifact in
an example scenario. Using the definition by \textcite{Peffers2012}, a prototype
is an implementation of the artifact that is used to demonstrate its utility
using illustrative scenarios. Scenarios are used to apply the artifact in
synthetic or real world situations to demonstrate its applicability.

DSRM is built around six steps where each of the steps have an output that is
used as an input for the next step. The flow of the research process is
demonstrated in \autoref{fig:dsrm}, while the specific DSRM steps are listed
and described in order in the following list \parencite{Peffers2007}:

\begin{enumerate}
  \item \textbf{Problem identification and motivation:}
  In the first DSRM step, the research problem is defined and justified.
  Clearly defining the problem and the concepts related to it helps digesting
  the complexity of the problem domain. Justifying why the problem should be
  solved provides insight to the line of reasoning of the researcher and
  provides motivation by communicating the value of the solution. This is the
  focus of the \autoref{chap:introduction}, as we introduce the problem of
  data marketplaces and motivate how provenance might be able to solve it.
  \item \textbf{Define the objectives for a solution:}
  The problem definition is used to \textit{infer} objectives that can be
  used to measure the solution. These objectives can be either qualitative or
  quantitative. Qualitative objectives are similar to functional requirements
  found in engineering disciplines by describing how the solution would
  address the problem through its functionality. Quantitative objectives, on
  the other hand, reflect non-functional requirements that measure specific
  metrics, such as speed or performance, and can be directly benchmarked
  against other solutions. After describing the background theory in
  \autoref{chap:marketplaces}, \autoref{chap:data-as-a-service}, and
  \autoref{chap:provenance}, we introduce a concrete list of objectives in
  the form of requirements at the beginning of \autoref{chap:empirical}.
  \item \textbf{Design and development:}
  Using the \textit{theory} created in the previous step, the artifact is
  created and the reproducible steps to recreate it are documented in a
  manner that fits the type of the artifact. There are many possible
  artifact types that can be created in the process. To mention a few, an
  artifact can be a concrete instantiation, a model, a construct, or a
  method for doing something. A software-based artifact, for instance, could
  include activities of determining artifact's functionality, forming an
  architecture, and lastly creating an instantiation based on the
  architecture. In this thesis, we form our own data model based on PROV and
  design the components that support its function in the context of data
  marketplaces after defining the requirements in \autoref{chap:empirical}.
  The data model is described in \autoref{sec:development}.
  \item \textbf{Demonstration:}
  The artifact (\textit{How to knowledge}) and its capability of solving a
  problem is demonstrated with one or more applications that are applicable
  to the nature of the artifact. Some of these include case studies, proofs,
  simulations, and experiments. The data model created in this thesis is
  tested using a prototype application, with the process being detailed in
  \autoref{sec:demonstration}.
  \item \textbf{Evaluation:}
  Lastly, the performance and applicability of the artifact in solving the
  problem is evaluated using \textit{metrics, analysis and knowledge} gained
  from the demonstration. As with the earlier steps, the method of evaluation
  that best fits the nature of artifact should be chosen. The results and
  insight obtained during design, development and demonstration of the
  metadata model are discussed in \autoref{sec:evaluation}.
\end{enumerate}

\begin{figure}[h]
\begin{center}
\begin{tikzpicture}[node distance=2.5cm, every node/.style={fill=white}, align=center]
  % Specification of nodes (position, etc.)
  \node (identify)             [process]              {Identify problem \& Motivate};
  \node (define)     [process, below of=identify]          {Define objectives of a solution};
  \node (design)      [process, below of=define]   {Design \& Development};
  \node (demonstration)     [process, below of=design]   {Demonstration};
  \node (evaluation)      [process, below of=demonstration] {Evaluation};
  \node (communication)      [process, below of=evaluation] {Communication};
  % Specification of lines between nodes specified above
  % with aditional nodes for description 
  \draw[->]     (identify) -- node {Inference} (define);
  \draw[->]     (define) -- node {Theory} (design);
  \draw[->]     (design) -- node {How to knowledge} (demonstration);
  \draw[->]     (demonstration) -- node {Metrics, analysis \& knowledge} (evaluation);
  \draw[->]     (evaluation) -- node {Disciplinary knowledge} (communication);
  \draw[dashed,->] (evaluation) -- +(4,0) |- node [near start] {Iterate}(design);
  \draw[dashed,->] (communication) -- +(4,0) |- node [near start] {Iterate}(define);
\end{tikzpicture}
\end{center}
\caption[DSRM Process Model]{DSRM Process Model by \textcite{Peffers2007}.}
\label{fig:dsrm}
\end{figure}

Although the model appears linear, it provides flexibility by allowing the
research to enter the process from later steps, and giving an opportunity to
iterate to earlier steps when necessary. By allowing multiple entry points,
DSRM can also be applied to research where objectives have already been
defined in some other context (Objective-centered solution), or situations
where an artifact already exists, but is not used in specific problem context
(Design and Development centered approach), or lastly, when the research
simply observes a practical solution and how it works (Client/context
initiated solution). In this thesis, however, we follow DSRM from the very
first step.

Iteration in the DSRM process can occur at evaluation and communication steps
if the nature of the research allows it. A researcher might, for instance,
return backwards in order to improve or build on the solution, or find an
entirely different approach that solves the problem. The new solution could
be benchmarked against the earlier solution to discover whether an
improvement was made. Additionally, some researchers might choose to pursue
an iterative search process in the design and development phase of the
artifact. \parencite{Peffers2007}