\chapter{Discussion}

In this thesis, a metadata model for data marketplaces was proposed, enabling
tracking of provenance and metadata of hybrid data products. The metadata
model was implemented in a functional prototype to investigate its viability
and to evaluate the performance impact of the metadata model through a set of
benchmarks. The research was performed according to the steps of design
science research method (DSRM) that also provided the general structure of
the thesis. From the perspective of the author, DSRM was a fitting choice as
it helped narrowing the scope of the thesis to a problem area that was still
feasible to evaluate. The resulting metadata model was successfully able to
fulfill its requirements without causing a considerable performance impact.

At the time of writing this thesis, research on data marketplaces
was still scarce, but sufficient to provide background. Academic sources were
prioritized in the writing of the thesis, but for topics that discussed
technical and implementation details, non-academic sources were more relevant
and available. Even when academic sources were not used, we attempted to use
literature sources that were highly regarded by the software developer
community.

The scope of the metadata model was restricted to only cover hybrid data
products and the data sources they originate from. Additionally, the
definition of data marketplace metadata was restricted to data contract terms
of \textcite{Truong2012}. It is likely that the model can be both extended
with new concepts in the future, and made more complete by adding additional
metadata attributes. Doing so, however, would require additional insight to
data marketplaces and their needs. This thesis was constrained by the lack of
literature, but future research might enable formulation of more
comprehensive metadata models.

The PROV model provided a suitable base for building the metadata model, and
it seems to also have been used in non-academic contexts
\parencite{KingsCollegeLondon2018}. In the development of the prototype,
however, it felt that some implementations of PROV have received more
attention than others. Although the JavaScript implementation of PROV,
ProvJS, was advertised in \parencite{KingsCollegeLondon2018}, it's not
mentioned in W3C specification \parencite{Gil2013}. Additionally, ProvJS did
not have documentation on its usage and only provides example code snippets
in its repository. Based on this, it might be preferable to develop future
PROV solutions using implementations made for other languages.

To measure the performance of metadata model, a benchmark for provenance
systems by \parencite{Tas2016} was adapted for the purposes of this thesis; a
benchmark that has also been used for other provenance systems. In addition
to the three metrics provided the benchmark, including latency, message rate
and simultaneous connections (pipelines), a fourth metric of filesize was
included to represent the volume of metadata model. The benchmarks were
conducted using consumer hardware, on a single machine, and without
multi-threading. Although the benchmarking environment was not similar to
cloud computation environment that favors multi-threading and distributed
computation \parencite{Koutroumpis2017}, we believe the metrics to be
sufficiently valid in the context of this thesis. The relative performance
impact of the metadata model should should not vary significantly between
environments because the PROV-JS library used in the prototype is not
parallellized and gains no performance benefits in server environments.

The thesis and the prototype source code is hosted on gitlab at
\url{https://gitlab.com/duics/gradu} where the results of the benchmarks can
be independently verified for as long the APIs that the prototype relies on
are still available. However, unless the benchmark environment is identical,
it's likely that the results are nonidentical. Regardless, relative
performance differences between when metadata is enabled and when metadata is
disabled should still remain approximately same.

The performance of the metadata model relied highly on the NodeJS runtime and
ProvJS library provided by \textcite{KingsCollegeLondon2018}. Although the
tests where network requests were enabled in \autoref{sec:evaluation}
indicated that the performance impact of the metadata model was negligible in
that specific scenario, there might be use cases where sub-millisecond
latencies are required. it might be possible that PROV libraries designed for
other programming languages are more performant. Additionally, it might be
possible to improve performance of the metadata model by using binary encoded
formats, such as Protocol Buffers or Apache Trift \parencite{Kleppmann2017}.
Both of these encodings trade flexibility for higher performance, leading to
additional design constraints in the metadata model. In addition to
potentially allowing the metadata model to be used in scenarios where
sub-millisecond latencies are a requirement, such approaches might also be
useful in systems that encounter very high traffic where even a minor
performance increases can lead to notable savings on processor time.

At an earlier time at the writing of the thesis, an alternative perspective
of investigating architecture of a data marketplace was also considered.
Currently there exists little research on how one might build a data
marketplace or a Data as a Service (DaaS) platform while taking into notion
the special requirements set by those specific services. Stream processing
and event-based architectures have recently become more commonplace in the
industry due to their capability of handling real-time data with simple
abstractions \parencite{Kleppmann2017}, leading to an abundance of
technologies that can be used to facilitate the technical architecture of a
stream data marketplace. Although the challenges of scalability and
adaptability of data marketplace architecture are interesting, investigating
such a broad topic would not have been viable in the limited context of a
master's thesis. The idea could, however, be applicable in other contexts.

The topic of data marketplaces in general has many potential branching
directions for future research. As the concept of a metadata is ubiquitous on
data marketplaces, there is potential in developing the metadata model
alongside the new research topics. \textcite{Koutroumpis2017} suggests two
major future research topics for data marketplaces: Creation of \textit{data
contract management} systems for managing and enforcing contractual terms and
conditions for data transactions and \textit{distributed data marketplaces}
that require no central authority.

\textcite{Koutroumpis2017} suggests that a data contract management system
would require a \textit{data contract clearance service} to generate
contractual terms to hybrid data products. The contractual terms of the
hybrid data product would be generated from the derived sources based on the
contractual logic of each data source. The idea was experimented with in the
prototype created during the empiric part of the thesis: A hybrid data
product derived from multiple sources was `priced' according to the cost of
each source used in the generation of hybrid data product. However, applying
similar logic to other data contract terms would require investigating and
applying contractual law: A topic that was out of scope in this thesis.
Future research could include investigating how contractual terms could be
inherited by hybrid data products.

Research on distributed data marketplaces is closely related to blockchains
and distributed ledgers \parencite{Koutroumpis2017}. Distributed marketplaces
might require adjustments to the metadata model described in this thesis, or
possibly an entirely different approach to metadata. Depending on how a
distributed marketplace is built, provenance information could even be
deferred from blockchain transactions themselves. As distributed marketplaces
have no central authority that manages the data marketplace, transactions and
metadata related to them has to be shared with all participants of the data
marketplace. This would allow each participant to verify trustworthiness of
other actors. However, the transparency of blockchain also has privacy and
security concerns as none of the transactions on the blockchain are
anonymous. A decentralized marketplace would, in essence, just provide the
communication structures that facilitate operations of a decentralized market
\parencite{Koutroumpis2017}. Another possible alternative to distributed data
marketplaces could be found in secure Multi-party Computation (MPC)
architecture. In a secure MPC architecture, the trusted middleman is
"emulated" through cryptographic interactions \parencite{Goldreich1998}, but
there exists no earlier research on how the concept could be applied to data
marketplaces.

In this thesis data marketplaces were discussed as a platform to facilitate
interactions between organizations that seek to trade information with each
other. However, \textcite{Kortuem2010} suggest that, with the growth of
Internet of Things, even consumers might be incentivized to take part in the
data markets. There exists some research that points towards the willingness
of consumers to share data from IoT devices. Research by
\textcite{Foster2009,Foster2010} indicates that consumers are willing to
share data on their electricity usage. Additionally, the findings of
\textcite{Grossklags2007} point towards consumers having a high preference of
giving up private information for monetary gain. In future, this could enable
data marketplaces where consumers could trade their private data as a
commodity. Companies could then use that data to provide services to the
consumers, or use that data in exchange for monetary compensation to the
user. To maintain metadata and provenance of data products in such data
marketplaces, the metadata model would have to be extended with agents that
represent the consumers while still remaining compliant with privacy
regulations.

Common to all future research directions, it might be preferrable to develop
a metadata model in cooperation with data marketplaces themselves. This would
likely lead to an interesting metadata model, as it would be closely aligned
with requirements and practices of the industry.