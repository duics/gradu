\section{Stream processing systems}

\subsection{Apache Kafka}

Apache Kafka is a distributed message broker originally developed at LinkedIn
to handle communication between different data sources, applications and
storages. It has the capability of transforming the data to appropriate formats
and handle certain Stream Processing operations. To manage its internal
processes, Apache Kafka is deeply coupled with Apache ZooKeeper, which was
first introduced by \cite{Hunt2010}. ZooKeper is a highly performant
distributed coordination service for managing messaging, registers and locks.

\cite{Narkhede2017} compares the operating logic of Kafka with the
well-known publish/subscribe pattern in programming: Similar to subscriptions
to email lists or newspapers, the readers (i.e. consumers) ask the publisher
to send them updates on specific topics. When something new related to that
topic happens, a message is dispatched to everyone who was subscribed to that
topic.

Unlike other frameworks listed, Kafka is designed for compatibility. It acts
both as a hub for distributing real time data and storing it in a form of a
commit log. Commit log is written to be immutable with its append-only
semantic. In other words, once a message is passed to Kafka, it's written to
the end of the log and it can never be changed. This allows preserving
message history. If other services connected to Kafka would fatally crash and
lose all their data, it can be reconstructed by simply rewinding and
replaying messages from Kafka's commit log.

In Enterprise world, Kafka solves similar problems as ETL
(Extract-Transform-Load) and ESB (Enterprise Service Bus). Both ETL and ESB
are used to move data from system to system, which warrants a comparison to
Kafka. The main difference comes from the scalability and flexibility of
Kafka. ETL is typically used to do batch-type jobs that move data between two
predefined systems, while Kafka allows and Kafka is clearer of the two: ETL
is used to create data pipes between one or multiple systems, and the data is
typically moved in batches. ESB, on the other hand, was tied with the
concept of Service-oriented architecture, which resulted in the systems being
massively complex and hard to maintain due to interdependencies between
different services. \parencite{Dunning2016}

Unlike traditional approaches used by ``pulling'' data, it's meant that
instead of sending data at a constant rate, the applications pull additional
data themselves as they need it. This helps prevent situations where an
overloaded service would have to drop some requests. Many Stream Processing
systems have been built with Kafka in mind due to the reliability guarantees
it provides. \parencite{Narkhede2017}


\begin{figure}[h]
  \includegraphics[width=10cm]{kuviot/kafka-apis.png}
\caption[Kafka connectors]{Different types of connectors with Kafka \parencite{Narkhede2017}}
\label{fig:kafka-connectors}
\end{figure}


Kafka is built around the concept of few key concepts:

\begin{description}
    \item[Message]
     A unit of communication, which consists of content and metadata. Kafka
     does not concern itself with the content of the message, but the
     metadata included with the message is used to help with internal
     partitioning.
    \item[Topic]
    Topic is simply a channel through which certain messages are piped
    through. Consumers can then decide which topics to listen.
    \item[Producer]
    Producers emit messages to Kafka which are dispatched to consumers
    through topics.
    \item[Consumer]
    Consumer is a client that reads messages. A consumer may subscribe to one or
    more topics to receive messages from them. 
    \item[Connectors]
    Connector is an alternative concept to the client/producer model. It's
    used to move data between Kafka and alternative data storages, such as
    relational databases or caches. 
\end{description}

% Kafka fulfills the rules set by \parencite{Stonebraker2005}

% \begin{table}[h!]
% \caption[Kafka ]{Eight rules for stream processing}
% \begin{tabularx}{\textwidth}{|l|X|}
%   \hline
%   \textbf{Rule} & \textbf{Kafka} \\ \hline
%   \makecell{Keep the Data Moving} &
%   Kafka processes messages instantly
%   \\ \hline
%   \makecell{Query using SQL \\ on Streams (StreamSQL)} &
%   Kafka features its own StreamSQL syntax called KSQL. However, at the time
%   of writing this thesis, KSQL was still under development.
%   \\ \hline
%   \makecell{Handle stream \\ imperfections} &
%   KSQL has worker queues with timeouts
%   \\ \hline
%   \makecell{Generate predictable \\ outcomes} &
%   Kafka is deterministic
%   \\ \hline
%   \makecell{Integrate stored \\ and streaming data} &
%   In addition of maintaining the old messages in a commit log, Kafka can
%   integrate data from external databases.
%   \\ \hline
%   \makecell{Guarantee data safety \\ and availability} &
%   Kafka is highly reliable with its Zookeeper architecture and is able to
%   maintain operation even if some of the nodes fail.
%   \\ \hline
%   \makecell{Partition and scale \\ application automatically} &
%   Kafka can dynamically adjust its scale with ZooKeeper.
%   \\ \hline
%   \makecell{Process and \\ respond instantaneously} &
%   Kafka can process data in microseconds. However, this also depends on the
%   type of workload required in the processing phase.
%   \\ \hline
% \end{tabularx}
% \label{tbl:stream-rules}
% \end{table}

\subsection{Apache Flink}

Apache Flink was originally developed under the name of Stratosphere as a
joint research project between few European Universities between 2010 and
2014, after which it was accepted as an Apache incubator project. The development of
the framework is closely tied with a company ``data Artisans'' which was
founded by the framework's original contributors.
\parencite{Friedman2016}

Flink is capable of both low-latency real-time processing and batch
processing, which makes it a hybrid processing platform
\parencite{Lopez2016}. It includes core libraries for Complex Event
Processing, machine learning, and graph processing, which are available
through Java and Scala API's \parencite{Dunning2016}. Although Flink
is not as mature as other frameworks, it can be used in wider range of
applications than the other alternatives due to its capability of processing
batches similarily to stream data \parencite{Friedman2016}.

Flink has two distinct API:s. DataStream API intended for real-time stream
data, and DataSet API that is used for batch processing. Flink handles batch
processing as a special case of stream processing -- a batch is simply a
stream with finite data. Although the underlying processing engine is the
same for each API, the middleware in API:s enables features that are
beneficial for each use case. DataSet API middleware has additional query
optimization, backtracking recovery and external memory structures. Meanwhile
middleware in DataStream API allows for checkpointing, state management,
watermarks and windows with triggers. \parencite{Friedman2016}

The processing architecture of Flink can be seen in
Figure~\ref{fig:flink-architecture}. Flink jobs are compiled into a directed
graph of tasks, which are assigned with a master-worker model. A mananger
coordinates tasks and maintains a state of the system while workers (known as
Task Managers in Flink) handle the grunt processing. In the Flink model, Task
Managers also communicate with each other to share intermediate results.
Additionally, a heartbeat mechanism is used to keep the system in sync and
send statistics to job manager. \parencite{Lopez2016}

\begin{figure}[h]
  \includegraphics[width=12cm]{kuviot/flink-architecture.png}
\caption[Flink components]{Processing architecture in Flink \parencite{Lopez2016}}
\label{fig:flink-architecture}
\end{figure}

\subsection{Apache Storm}

Apache Storm is one of the more established real-time processing frameworks
that was designed to integrate into the existing batch-based Hadoop
ecosystem. It only offers at-least-once guarantee out of the box, but
exactly-once guarantee is achievable by using an additional component called
Trident. Although Storm is not capable of batching, Trident achieves
exactly-once processing with a micro-batching mechanic, which results in a
performance penalty \parencite{Allen2015}. Due to this Trident is included
as an optional component. \parencite{Dunning2016}.

In addition to JVM languages, Apache Storm can be programmed from any
language that implements a binary protocol called Apache Thrift. With other
frameworks, such as Apache Flink, the developer is mostly limited to
languages that run on JVM (Java Virtual Machine). Thrift bypasses this
limitation by simply enabling a channel of communication to Storm with remote
procedure calls. \parencite{Allen2015,Jain2014}

\begin{figure}[h]
  \includegraphics[width=8cm]{kuviot/storm-topology.png}
\caption[Storm topology]{Storm Topology \parencite{Jain2014}}
\label{fig:storm-topology}
\end{figure}

According to \cite{Allen2015}, Storm has the following key
abstractions for its processing model, which are also visualized in
Figure~\ref{fig:storm-topology}:
\begin{description}
  \item[Topology]
  Topology is a graph that represents the processing pipeline. Nodes in the
  graph are intermediate computations, and edges illustrate how the results
  move to next computations.
  \item[Tuple]
  The basic unit of communication in Storm: Tuple is a list of fields that
  contain a value. The value can be a typical value included in the Java
  standard libraries (e.g. char, int, double), or a custom data type defined
  with Storm API. \parencite{Jain2014}
  \item[Stream]
  A stream is an ``unbounded sequence of tuples'' \parencite{Allen2015}. A
  flow of tuples from one node to another in the Topology -- The stream of data
  that moves between computations.
  \item[Spout]
  The source of data that begins the data pipeline. Spouts typically read
  data from an external source, and pass it along to following nodes without
  doing any processing.
  \item[Bolt]
  Bolt is a node that takes in tupes and performs a computations on it, and
  outputting results as tuples for the next node.
\end{description}

Storm uses a master/worker architecture, where the master node is known as
Nimbus, and worker nodes are known as Supervisors. Storm architecture relies
on ZooKeeper similarily to Apache Kafka. ZooKeeper is used to coordinate
communication and state between Nimbus and the supervisors.
\parencite{Allen2015}


\subsection{Apache Spark Streaming}

Spark Streaming belongs to the family of Spark projects that originated from
UC Berkley. Spark is built on JVM and is programmable with Java, Scala or
Python. Spark Streaming is an extension to Spark that enables it to process
stream data with micro-batches -- performing computations only on small
groups of messages (see Figure \ref{fig:spark-flow}). Using micro-batches
seperates it from `truly' real-time frameworks that process one message at a
time. The approach is opposite from Flink which considered batches a special
case of stream data. Spark Streaming considers streaming a special case of
batches, which allows code reuse from the mainline Spark project. Due to this
property, Spark Streaming is not fit for low-latency computations, and an
alternative framework should be used for those use cases.
\parencite{Dunning2016}

\begin{figure}[h]
  \includegraphics[width=10cm]{kuviot/spark-streams-flow.png}
\caption[Spark Streaming processing flow]{Spark Streaming processing flow \parencite{Hesse2015}}
\label{fig:spark-flow}
\end{figure}

Similar to both Flink and Storm, Spark streaming also uses the master-worker
model \parencite{Lopez2016}, The main abstraction behind Spark is
`resilient distributed datasets' (RDDs). RDDs are collections of data objects
that are created by doing computations on data. RDDs form a computation graph
that preserves the steps that were used to generate the data contained in the
RDD \parencite{Zaharia2012}.

One benefit discovered by \cite{Lopez2016} is that Spark
Streaming is highly resilent -- In case of a node failure, Spark Streaming
didn't lose any messages while in a similar situation Apache Flink lost
12.8\% of messages and Storm lost 22.2\%. This might make Spark Streaming a
better choice for applications that absolutely require processing all
messages.

\subsection{Apache Samza}

Apache Samza was developed alongside with Apache Kafka at LinkedIn. Due to
this, the projects are by design interoperable and commonly used together. In
addition to Kafka, Samza is commonly used with cluster manager software
called Apache YARN (Yet Another Resource Negotiator) developed originally at
Yahoo! \parencite{Vavilapalli2013}. Samza runs on JVM, and is
programmable with JVM-based languages. Samza also provides a Streaming SQL
engine called SamzaSQL, which enables using SQL semantics for building
streaming architectures. \parencite{Pathirage2016}
\parencite{Hesse2015}

\begin{figure}[h]
  \includegraphics[width=8cm]{kuviot/yarn-architecture.png}
\caption[YARN Architecture]{YARN architecture \parencite{Hesse2015}}
\label{fig:yarn-architecture}
\end{figure}

In YARN, each node (i.e. running instance of a machine) in a computation
cluster runs a Node Manager that will start application containers based on
the instructions of the Resource Manager. Node Managers communicate with
Resource Manager to stay in sync with a heartbeat mechanism. Node Managers
also communicate with each other, but only at application level. Samza
handles communication at application level and provides the computational
framework for stream processing. When combined with Kafka, Samza provides an
at-least-once processing guarantee. \parencite{Hesse2015}

\subsection{Apache Apex}

Similar to Apache Flink, Apache Apex is a stream processing engine that is
capable of both batch and stream processing. It's based on a commercial
proprietary technology called DataTorrent RTS, which was open-sourced and
incubated at Apache in 2015. Similar to Samza, it's built with YARN
architecture in mind. Apex provides Exactly-once processing guarantee.
\parencite{Dunning2016}
