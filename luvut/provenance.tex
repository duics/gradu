\chapter{PROVENANCE}
\label{chap:provenance}

The second chapter provided details of data marketplaces and their special
requirements. The third chapter introduced the concepts of DaaS, properties
of data, and stream processing. In this chapter, we introduce concepts of
provenance, investigate how provenance reflects to data, and discover what
existing solutions exist for tracking provenance. Some literature used in
this chapter was discovered on search engines using relevant keywords, while references to W3C
specifications were gathered directly from the source.

The concept of provenance has been used in the context of art to preserve
history of artifacts, denoting the lineage of ownership for objects. The
provenance record is used by scholars and collectors to verify the origin of
art, its authenticity, and its price \parencite{Deelman2010}. Likewise, the
concept of provenance has been used in digital libraries to document the
usage history and life cycle of digital books and documents
\parencite{Moreau2008}.

When applied to data, provenance refers to the origin of data in terms of
time and place. It answers the questions of how was data created or
generated, what intermediate processing steps have occurred to the data, and
through which parties has the data passed through
\parencite{Lim2009,Deutch2018}. The questions are important in order to
understand the properties of data that originate from multiple sources.
Provenance becomes increasingly important as the complexity and the amount of
different data sources rise, as tracking the origin of data would become
more and more challenging \parencite{Deutch2018}. Provenance is also a useful concept
for scientists as it can be used to ensure reproducibility of data analysis.
Provenance can indicate how the results of the research were derived, with
what parameters, and from which data sets \parencite{Moreau2008,Deelman2010}.
Provenance of data has applications in diverse range of different domains.
\textcite{Lim2009} provides an example of a situation where provenance of
data is vital in order to ensure that correct decisions are made in life and
death scenarios:

\begin{displayquote}
``A battlefield monitoring system gathers enemy locations from various
sensors deployed in vehicles, aircrafts, and satellites and processes the
monitoring queries over these streaming data. In this system, we need to
assess the collected data since we must make sure that mission critical
applications only access highly trustworthy data in order to guarantee
accurate decisions by these applications. Since sensors and communication
lines have different accuracy and confidence, it is essential to know the
provenance of each data for assessing its trustworthiness level.''
\end{displayquote}

Being able to track provenance is helpful for ensuring quality of results,
enforcing policies and ensuring trust in the system
\parencite{Deutch2018}. When extended to data marketplaces, data provenance
has a crucial role of linking data back to the data providers where the data
originated from. The value of the data on a data marketplace is directly
linked to data provenance as it provides information quality and legality of
data \parencite{Koutroumpis2017}.

Provenance data is contained within metadata. Metadata, as defined in the
introduction, is ``structured data about an object that supports functions
associated with the designated object'' \parencite{Deelman2010}. In other
words, metadata can be used to contain information that is not part of the
data itself, but that can be used to support and compliment the operations
and processes related to the data. Metadata is organized in a set of
attributes that belong to a specific schema. A schema can be an application
specific practice or a commonly used standard, that dictates the names of the
attributes, the type of data that is expected to be found from those
attributes, and when and how should the attributes be used.
\textcite{Deelman2010} provides some general metadata categories that are found
from various metadata schemas used by scientific applications:

\begin{itemize}
  \item \textit{Logical file metadata},
  metadata related to files that, for example, includes logical file names,
  data types, and creation and modification timestamps.
  \item \textit{Logical collection metadata},
  metadata on collections of files or other data, including data on
  collection name, collection contents, creator, modifiers, and audits.
  \item \textit{Logical view metadata},
  metadata on the logical files, collections and other views that are used to
  form a specific presentation to data
  \item \textit{Authorization metadata},
  metadata on who is allowed to access the data (i.e. access permissions)
  \item \textit{User metadata},
  metadata on the users of data, including names, phone numbers and addresses
  \item \textit{User-defined metadata},
  custom metadata attributes that have been defined by the users of metadata,
  extending the schema onto which the metadata is based on.
  \item \textit{Annotation attributes},
  metadata in the form of unstructured text written by the user to describe
  data or its attributes.
  \item \textit{Creation and transformation history},
  provenance metadata on how data was created, and what type of
  transformations were performed to the data.
  \item \textit{External catalog metadata},
  linking to metadata that is available from an external resources.
\end{itemize}

The concept of provenance has been investigated at multiple different levels
of abstraction where each domain has its own challenges. \textcite{Herschel2017}
classified provenance research to four different categories. The categories
can be placed to a hierarchy according to granularity, how specific the
domain is, and how high the level of instrumentation available for collecting
provenance for that type is. We first start from the most fine-grained
category, and move upward to less strict categories.

\begin{description}
  \item[Data provenance]
  The highest resolution of provenance tracking that allows tracking of
  individual data items and the operations that they go through. The level is
  typically viable only for structured data models and declarative query
  languages (e.g. SQL) as they allow highest possible degrees of
  instrumentation with clearly defined semantics and operators. Data
  provenance research is divided to two categories: Provenance of
  \textit{existing results} that detail provenance of data that was given out
  as a result, and \textit{missing results}, providing provenance to data
  that was left out of the results, and more specifically, explaining why it
  was left out. Provenance of existing results seek to answer three main
  questions: \textit{where} does the data originate from, \textit{why} were
  they included in the result, and \textit{how} was the data processed or
  modified in order to provide the given result.
  \item[Workflow provenance]
  Instead of observing individual data, workflow provenance covers inputs,
  processes, and outputs of systems. Workflow provenance can be presented as
  a directed graph, with processes being nodes of the graph, and edges
  connecting them to form the workflow. The level of granularity of the
  provenance can vary between solutions. Some solutions might track
  provenance at the level of individual data items, while others are more
  coarse and are not concerned with individual changes to data. Fine-grained
  workflow provenance is close to data provenance, but does not necessarily
  fulfill the requirement of having operators with clearly defined semantics.
  \item[Information system provenance]
  Information system provenance is metadata about processes that belong to
  an information system and have the role of storing, communicating or
  distributing information. Information system provenance generally considers
  all processing steps as "black boxes", but may use input, output and the
  parameters of the process to collect provenance. One example of information
  system provenance is provenance-aware storage systems (PASS)
  \parencite{Muniswamy-Reddy2006} that track provenance of files and
  documents through metadata. PASS is also a valid example of provenance
  tracking on level of the operating system \parencite{Carata2014}.
  \item[Provenance metadata]
  The most general category of provenance that includes any arbitrary
  provenance related metadata. As a "catch-all" category, no assumptions or
  restrictions are made regarding how provenance is defined, collected, or
  modeled. The only requirement is that the metadata must be intended for
  purposes of provenance. Proprietary provenance solutions typically fall
  into this category as their internals are not publicly disclosed, and
  therefore their mode of operation can't be assessed.
\end{description}

Concepts of both data provenance and workflow provenance are both highly
specific to the domain of databases and workflow system
\parencite{Herschel2017}, but the categories of information system provenance and
provenance metadata are flexible enough to be used in multiple different
scenarios. In the following section, we investigate an existing
provenance metadata model.

\section{Open Provenance Model and PROV}

\noindent
Open Provenance Model is a model collaboratively designed by groups of
researchers from various disciplines with the shared goal of attempting to
find a standard a model for provenance \parencite{Moreau2011}. After the
model reached sufficient maturity, it cornered enough support to form a base
for a W3C (World Wide Web Consortium) specification. The model was
implemented by W3C Provenance Group in the form of PROV, a concrete data
model and technical specification \parencite{Moreau2015} that can be used to
build applications that use provenance metadata.

Open provenance model was designed to fulfill the following requirements:

\begin{enumerate}
  \item The model must allow sharing provenance information between different
  systems with means of a shared provenance model
  \item The model must allow for development and sharing of tools that
  operate on the model
  \item Provenance in the model must be defined precisely, but without
  describing a specific technical implementation
  \item The model must be able provide a digital representation of provenance
  regardless of the nature of the thing being observed
  \item The model must allow for multiple levels of detail that can coexist
  \item The model must define set of core rules that can be used to identify
  and infer relationships
\end{enumerate}

To address these requirements, the group opted for a representation in the
form of directed graph, and defined a set of semantics and rules for the
nodes, edges, and their composition. There are three types of nodes. Firstly,
\textbf{Artifacts} denote a state, representing a physical object, or a set
of data. Artifacts are represented as circles. Second, \textbf{Processes}
represent actions that are performed on artifacts (or due to them), resulting
in new artifacts. Processes are denoted as squares. Third, \textbf{Agents}
are entities in the process that facilitate, control or affect its execution.
Agents are represented as octagons. Nodes are linked together with edges,
represented with arrows, that communicate a relationship between each of the
linked nodes. Some of the common relationships include \textit{derived from},
when the artifact is derived from an earlier state, and \textit{triggered by}
when an agent or process has a causal effect to something. A basic example is
provided in \autoref{fig:opm-examples}, where a provenance graph of an
incrementing operator is modeled at two levels of granularity. Thin and
dotted arrows denote the direction of actions, while thick arrows represent
derivation relationships. Although the graphs are different, they both
observe the same events. In the context of OPM, these are considered a two
related \textit{accounts} observed from different perspectives. \parencite{Moreau2011}

\begin{figure}[h]
  \begin{center}
  \includegraphics[width=\textwidth,keepaspectratio]{kuviot/opm-examples}
  \caption[Examples of provenance graph]{Examples of a provenance graph \parencite{Moreau2011}}
  \label{fig:opm-examples}
  \end{center}
\end{figure}

The OPM specification provides a generic set of concepts and tools, but also
provides freedom to extend the model for practical domain specific problems.
OPM provides two mechanisms for this in the specification: \textit{OPM
annotation framework} and \textit{OPM profiles}. \parencite{Moreau2011}

OPM annotation framework allows arbitrary information to be attached to
relations, entities, agents, edges, and even other annotations. Annotations
in OPM are simply a special class of OPM entities that can also have their
own relationships and chain of provenance. An annotation entity must contain
\textbf{a)} an identifier that allows linking it with the annotated object,
\textbf{b)} A set of key-value pairs to contain the additional information
provided by the annotation, and \textbf{c)} the subset of accounts from the
annotated entity in which the annotation is relevant. \parencite{Moreau2011}

OPM profiles allows building extensions or specializations on top of OPM,
while still staying compatible with the general principles and graph
features. OPM profiles provide a tool for communities to create their own
best practices and usage guidelines that best fit the specific domain to
which the profile is applied on. An OPM profile consist of a unique global
identifier and contains one or multiple components. \parencite{Moreau2011}

\begin{enumerate}
  \item \textit{Controlled vocabulary for annotations:}
  Defining a set of key-value pairs, and the allowed types for values.
  Allowed values can be a predefined set (e.g. a key with the name "entrance"
  could only have values "open" or "closed"), or a general type (string,
  boolean, numeric etc.). The controlled vocabulary can be used to create
  custom types and add an arbitrary amount of domain-specific properties to
  nodes and edges.
  \item \textit{General guidance to expressing OPM graphs:}
  Guidance is used to provide instructions on the level of granularity, the
  level of expressiveness, and providing instructions for which types of
  nodes and edges should be present in the graph.
  \item \textit{Profile expansion rules:}
  Providing a set of rules that can be used to translate an OPM graph using
  the profile into a generic "profile expanded" OPM graph that requires no
  knowledge of the profile.
  \item \textit{Serialization specific syntax:}
  Instruction for serializing and deserializing the OPM graph. The component
  must explain how syntactic operations convert the graph into an arbitrary
  form of data, and how the data can be used to reconstruct the graph.
\end{enumerate}

The core of Open Provenance Model created a foundation for W3C PROV, a W3C
specification for a data model that enables provenance interchange on the Web
\parencite{Gil2013}. Similar to Open Provenance Model, PROV expresses
provenance in the form of entities and relationships between them, but
without the same focus in graphical representation. Instead, PROV is mostly
concerned with the concrete model of the data and how it can be used to
accommodate provenance data through multiple different perspectives. Some of
these perspectives include \textit{agent-centered provenance}, where
provenance is formed through interactions of people and organizations,
\textit{object-centered provenance} that follows the origin of documents or
data, and \textit{process-centered provenance} that focuses on actions and
processes used to generate and handle data or objects. The choice of
perspective depends on the view and the type of provenance information
required by the use case. \parencite{Gil2013}

To enable enough flexibility for various use cases, the PROV data model
(PROV-DM) features additional concepts that were not present in Open
Provenance Model, but also simplifies the model of extending and customizing
by allowing domain-specific information to be embedded more freely along with
the rest of the data model. PROV-DM has six conceptual categories that can be
used as building blocks for provenance metadata models, but in this thesis we
will only focus on the most vital three concepts that form the core of
PROV-DM that are also illustrated in \autoref{fig:prov-essentials} \parencite{Gil2013}:

\begin{figure}[h]
  \begin{center}
  \includegraphics[height=7cm,keepaspectratio]{kuviot/prov-essentials}
  \caption[PROV essentials]{Essential PROV components and their relationships \parencite{Moreau2013b}}
  \label{fig:prov-essentials}
  \end{center}
\end{figure}

\begin{description}
  \item[Entities and activities]
  Entities and activities are conceptually same as artifacts and processes of
  OPM. As described by \textcite{Moreau2013b}, ``an entity is a physical,
  digital, conceptual, or other kind of thing with some fixed aspects;
  entities may be real or imaginary''. Activities actions that can occur
  alongside with other activities, typically either \textit{generating} or
  \textit{using} entities, or \textit{communicating} with other activities.
  \item[Derivation]
  When an entity is altered or transformed into a new entity, it can be said
  that the new entity \textit{was derived from} the former entity. Derivation
  is used to preserve the chain of transformations so that the origin of data
  can be inferred later. Some examples of derivation include creating a
  painting from a canvas, melting ice into water, and modifying data in a
  relational database to a new form. \parencite{Moreau2013b}
  \item[Agents]
  An agent is something that has influence on entities, activities, or other
  agents. An agent could, for instance, be a person, an organization or a
  software program. The concept of agents is used to signify the effect that
  different agents could have to the outcome. PROV recognizes that an entity
  can be \textit{attributed} to an agent, agents can be \textit{associated}
  with actions, and agents can \textit{delegate} actions to other agents
  \parencite{Moreau2013b}. For instance, information that is attributed to a
  reputable news organization is more likely to be reliable than information
  that is attributed to a rumor circulating in social media.
\end{description}

These three concepts are codified to a syntax seen in the 
\autoref{tbl:prov-syntax}. The syntax can be used to generate a provenance record
as part of a software program when the data is being processed. Although we
will demonstrate the usage of PROV with pseudocode, PROV has bindings to
multiple different programming languages, including Python, Java and
Javascript \parencite{KingsCollegeLondon2018}. In the following pseudocode
example, we create a provenance record where an action \texttt{action1} took
an entity \texttt{example1} yesterday and finished transforming it to
\texttt{example2} today:

\begin{lstlisting}[language=PROV]
  entity(example1, [])
  activity(action1, now(), yesterday(), [])
  used(action1, example1)
  entity(example2, [])
  wasDerivedFrom(example2, example1, action1)
\end{lstlisting}

% TODO: Explain attributes
% TODO: Add agents to empirical?

\begin{table}[H]
\caption[PROV core syntax]{Core syntax of PROV and basic usage. Abbreviations
are expanded in \autoref{tbl:prov-abbreviations}. \texttt{\textbf{Bold}}
parameters indicate that they must be present, while those with
\texttt{normal} weight are optional. \parencite{Moreau2013b}}
% \vspace{2 mm}
{\renewcommand{\arraystretch}{1.5}%
\begin{tabularx}{\textwidth}{l|l|X}
\textbf{Category} & \textbf{Name} & \textbf{Syntax} \\
\hline \hline
\multirow{3}{2.5cm}{Entities / Activities} & Entity & \texttt{entity(\textbf{id}, \textbf{attrs})} \\
                       & Activity & \texttt{activity(\textbf{id}, \textbf{st}, \textbf{et}, \textbf{attrs})} \\
                       & Generation & \texttt{wasGeneratedBy(\textbf{e}, \textbf{a}, t, attrs)} \\
                       & Usage & \texttt{used(\textbf{a}, \textbf{e}, t, attrs)}  \\
                       & Communication & \texttt{wasInformedBy(\textbf{a1}, \textbf{a2}, attrs)}  \\
\hline
\multirow{3}{2.5cm}{Derivations} & Derivation & \texttt{wasDerivedFrom(\textbf{e2}, \textbf{e1}, a, g, u1, attrs)}  \\
                       &  &  \\
                       &  &  \\
\hline
\multirow{3}{2.7cm}{Agents, Responsibility, Influence} & Agent & \texttt{agent(\textbf{id}, \textbf{attrs})}  \\
                       & Attribution & \texttt{wasAttributedTo(\textbf{e}, \textbf{ag}, attrs)}  \\
                       & Association & \texttt{wasAssociatedWith(\textbf{a}, \textbf{ag}, pl, attrs)}  \\
                       & Delegation & \texttt{actedOnBehalfOf(\textbf{ag2}, \textbf{ag1}, a, attrs)}  \\
\end{tabularx}}
\label{tbl:prov-syntax}
\end{table}

\begin{table}[H]
\caption[PROV abbreviations]{Explanations for abbreviations used in \autoref{tbl:prov-syntax}.}
\begin{center}
\begin{tabular}{l|l}
\texttt{e} & Entity \\
\texttt{a} & Activity \\
\texttt{ag} & Agent \\
\texttt{pl} & Plan \\
\texttt{attrs} & Attributes (e.g. \texttt{[name="example"]}) \\
\texttt{id} & Unique identifier \\
\texttt{t} & Timestamp \\
\texttt{st} & Starting timestamp \\
\texttt{et} & Ending timestamp \\
\end{tabular}
\end{center}
\label{tbl:prov-abbreviations}
\end{table}

\section{Earlier PROV research and implementations}
\label{sec:earlier-prov}

\noindent
PROV has been used as a foundation for provenance metadata implementations at
many different domains. In this subsection, we list some of them and discuss
how they defined the requirements of the provenance model, and how the model
was evaluated. This subsection provides background and additional reasoning
for our custom implementation.

In the course of investigating existing solutions, it was found that
provenance systems have been investigated from multiple perspectives,
including usability, performance, and applicability. Both qualitative metrics
have been used in the form of interviews \parencite{Ramchurn2016}, and
quantitative metrics in the form of benchmarks \parencite{Baeth2017,Tas2016}.
The following list provides some examples of how PROV has been used in
different domains:

\begin{itemize}
  \item \textbf{A Disaster Response System based on Human-Agent Cooperation}
  \textcite{Ramchurn2016} developed a provenance model for tracking information
  from disaster sites. Requirements of the system were gathered through
  interactions with emergency responders, and it was found that a provenance
  system should be able to \textbf{1)} tell the source of information \textbf{2)}
  explain how that information was transformed into decisions \textbf{3)}
  link complimetary sources of information together. The system was evaluated
  by describing the system to focus groups, demonstrating the functionalities
  in rescue scenarios, and by gathering feedback.
  \item \textbf{Recommending Energy Tariffs and Load Shifting Based on Smart
  Household Usage Profiling} \textcite{Fischer2013} presented a system for
  personalized energy-related recommendations. In order to increase
  confidence of recommendations, provenance was implemented with the
  requirements of being able to \textbf{1)} justify why data was required for
  privacy reasons, and \textbf{2)} how data how the presented information was
  computed. The system was evaluated with data gained from semi-structured
  interviews of the users.
  \item \textbf{Modeling Information Diffusion in Social Media as Provenance
  with W3C PROV} \textcite{Taxidou2015} proposes a model known as PROV-SAID, an
  extended model of provenance based on PROV that is able to track the spread
  of information in social media (i.e. \textit{Information diffusion}). The
  objective of PROV-SAID is to provide as high expressive capability for
  information diffusion as possible, but the model is not tested.
  \item \textbf{A Large Scale Synthetic Social Provenance Database}
  \textcite{Baeth2017} created a synthetic database for storing provenance of
  social interaction on social media in standard PROV notation. The
  requirements were derived from provenance database requirements and
  usability. Lastly, the database was evaluated through synthetic benchmarks.
  \item \textbf{An Approach to Standalone Provenance Systems for Big Social
  Provenance Data} \textcite{Tas2016} contributes to provenance research by
  providing test suite to evaluate provenance systems, and by proposing a
  software architecture for a decentralized and scalable provenance
  management system to manage big social provenance data. The requirements
  were defined according to the constraints of big data, requiring high
  scalability and performance. Although the proposed architecture was not
  evaluated, existing provenance systems were benchmarked using the test
  suite.
  % Maybe also add the wikipedia example
\end{itemize}

\section{Summary}

\noindent
Provenance answers the questions of how, when, and by whom has the object of
interest been handled \parencite{Lim2009}. Provenance metadata encodes the
provenance information as a record alongside the object of interest. In the
context of data marketplaces, provenance metadata can contain information on
when the data was created, how it has been transformed, and through which
parties has the data passed through \parencite{Lim2009}. Provenance
information is vital for linking data back to its source and providing
details regarding the quality and legality of data
\parencite{Koutroumpis2017}.

Metadata is data that is not part of the data of interest, but is able to
support operations and processes related to it \parencite{Deelman2010}.
However, usage of metadata is not the only approach for tracking provenance.
In addition to provenance metadata, provenance can be tracked through
\textit{data provenance}, \textit{workflow provenance}, and
\textit{information system provenance} \parencite{Herschel2017}.

There exists a standard model for expressing provenance -- Open Provenance
Model (OPM) provides a set of building blocks that can be used to model
provenance through graphs. The provenance graph consists of three different
types of nodes: \textit{artifacts} denoting state, \textit{processes}
representing actions, and \textit{agents} being entities that factilitate,
control or affect execution of processes. The generic OPM concepts can be
extended to domain specific problems using either \textit{OPM annotation
framework} or \textit{OPM profiles}. \parencite{Moreau2011}

W3C PROV is an implementation of OPM, providing a framework and a syntax for
creating provenance documents using the programming language of choice. PROV
both extends OPM by providing additional building blocks for more complex use
cases, and simplifies it by allowing embedding of domain specific information
\parencite{Gil2013}. W3C PROV has been used in multiple different domains in
earlier research, and the resulting implementations have been evaluated either
with qualitative and quantitative metrics.