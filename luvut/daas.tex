\chapter{Data-as-a-Service}
\label{chap:data-as-a-service}

Data marketplaces provide platforms to trade data, but how is the trading
facilitated via internet? This chapter describes what Data-as-a-Service is
and how it fits into the current taxonomy of ``x-as-a-service'' landscape
originating from Cloud Computing. In order to understand the data that can
move within DaaS platforms, the chapter investigates data formats and the
concepts of big data, providing insight on the challenges of data processing
on DaaS platforms. In addition to using academic literature found from search
engines using relevant keywords, technical literature was used to support
some concepts. As much of the technical literature is not freely available,
the technical sources were chosen simply based on their available to the
author.

\section{Introduction to Data-as-a-Service}

\noindent
With the introduction of Cloud Computing, abbreviations such as SaaS
(Software-as-a-Service), PaaS (Platform-as-a-Service), and IaaS
(Infrastructure-as-a-Service) have become well known. Cloud computing moves
storage and computation to large data centers with leased services where
economies of scale increase efficiency over typical on-premise servers.
Service-oriented strategies have a longer history in manufacturing industry,
where the approach of finding competitive advantage through service-oriented
strategies is known as ``servitization''. An example of this is provided by
\textcite{Opresnik2015}: Hilti International, a drill manufacturing company,
created a new product that soon faced competition from another company with a
similar product at a lower price. To create a competitive advantage, Hilti
International transformed its drill into a service with a ``per hole''
pricing model. Service-oriented models enable a more economical approach
to computing: Services can be scaled up or down depending on the level of
workload, and instead of clients doing all the processing separately,
services can reduce redundant processing by serving precomputed data to
clients or move some computations entirely to the cloud
\parencite{Dikaiakos2009}.

Online data marketplaces are DaaS platforms. A DaaS platform can facilitate
trading of data through a set of endpoints that communicate via internet.
However, not all DaaS are necessarily data marketplaces, as some of them might
not have all the prerequisites to be called a data market. For instance,
\textcite{Stahl2016} suggests that in addition to providing data from
machine-readable endpoints, the service provider's primary business model also
needs to be centered around providing data and data-related services to be
classified a data marketplace.

DaaS platforms seeks to alleviate challenges that come from using data from
multiple sources. Different data sources may use different formats, which
requires additional engineering effort to integrate together. Standard
formats and interfaces are a requirement for enabling economies of scale as
they reduce costs of integration for all users of the service
\parencite{Assuncao2015}. DaaS platforms solve this by providing a generic
API that can be used for multiple independent data sources for common
operations (i.e. searching, downloading, uploading and updating), or through
a specialized API that operates over the otherwise fragmented data sources
\parencite{Vu2012}.

There exists various types of DaaS platforms, which tend to differ due to the
use cases and requirements of the service. One way to categorize DaaS
platforms is the amount of data assets provided on the platform, and the
relations between the assets. Based on this categorization, \textcite{Vu2012}
have observed three abstract types of services:

\begin{description}
  \item[Generic]
    A generic DaaS platform contains multiple independent data sets that have own APIs and
    properties. DaaS platforms are centered around data assets and APIs to access them, either
    using a generic or data asset specific API. Some examples of these include
    Amazon Data Sets and Microsoft Azure data marketplace.
  \item[Specialized]
    Specialized DaaS platforms are either centered around a single, or a
    limited amount of data assets. The API describes the DaaS platform and its services as
    a whole, and many operations that can be performed through the API work on
    the data assets collectively. Additionally, some specialized data assets can
    be manipulated through their API. A DaaS framework proposed in research such as
    \textbf{Active CTDaaS: A Data Service Framework Based on Transparent IoD in City
    Traffic} \parencite{Du2016} could be considered an example of a specialized
    DaaS, although without the capability of modifying the internal structure
    due to the immutable form of data.
  \item[Hybrid]
    A hybrid DaaS platform is an in-between model between Generic and
    Specialized DaaS approaches where an otherwise generic DaaS platform is
    used to provide access to data from a Specialized DaaS platform. The
    data from a Hybrid DaaS could be retrieved using either using a generic
    API provided by the DaaS platform or the API provided by the underlying
    Specialized DaaS platform.
\end{description}

The requirements set to DaaS platforms are closely related to requirements
set to big data systems. \textcite{Yin2015} provides an example of a
manufacturing company that gathers sensor data from their machines: A single
device alone generates 5000 data samples every 33ms which results in 150 000
samples in a minute, or total of four trillion samples per year. If the
company would, for example, want to sell the data from their machines to a
maintenance company, the DaaS platform used to facilitate the exchange of data
would have to be capable of fulfilling the same requirements as those in Big
Data systems.

Building a functional DaaS platform requires use of various technologies.
According to \textcite{Chen2011}, there are four categories of technologies 
that must sufficiently be covered for a DaaS platform to be successful:

\begin{description}
  \item[Data Modeling tools] A DaaS platform will handle data in multiple
    formats and schemas, which can make it difficult for users to understand and
    make use of the data. Data Modeling tools allow the data to be modeled and presented
    consistently, allowing users of the DaaS take advantage of various data sources
    more efficiently.
  \item[Common query language and API] A DaaS will be accessed by a variety of
    users, devices, and applications. For this reason, a single unified API and
    query language is essential: Firstly, it makes the cost of creating and
    updating applications that connect to DaaS as low as possible. Second,
    it separates the internal complexity of the platform away from the users. And
    third, the concern of generating low-level queries is moved to the
    DaaS itself, allowing queries to be automatically optimized.
  \item[Massive scale data management] The scale of data handled by DaaS
  should be assumed to be massive, requiring massive scale data management.
  To mention a few examples, issues of scale can be managed with replication of
  server nodes in order to respond to varying load, partitioning between
  geographical areas, and by ensuring backups and successful restoration of
  databases in case of errors.
    \item[Data cleansing and processing technologies]
    As DaaS typically provide a common API and query language, it must also
    have methods of ensuring that the data from a variety of different
    sources conforms to a similar schema. A range of data cleansing and
    processing services is therefore required to convert data to the desired
    schema.
\end{description}


\section{Properties and processing of data}

\noindent
A DaaS platform must be able to process data with different properties
\parencite{Chen2011}. In this section, we look at attributes of data, and the
constraints placed on handling data as the amount of data grows towards the
scale of big data -- high-volume, high-velocity and high-variety data that
requires innovative approaches to use it properly \parencite{Kleppmann2017}.

\subsection{Data formats and their structures}

\noindent
For data to be readable by computers, it has to conform to some type of
schema while preferably staying readable to humans as well. There exists
multiple data formats that can be used to transfer and store data on web.
The more structured the data structure is, the easier it is to process using
automated tools. \textcite{Assuncao2015} define the following four categories of
structuredness of data:

\begin{description}
  \item[Structured]
    Data that is modeled and follows a certain schema. Easier
    for computers to process, but not necessarily readable by humans.
    \textcite{Li2008} mention relational databases as a prime example of a
    structured data source.
  \item[Unstructured]
    Data that has no specified schema or structure (e.g. natural language, video
    audio etc.). Text documents are a good example of this: Easily read by
    humans, but a challenge for computers to correctly understand.
  \item[Semi-structured]
    Data that might have some structure, but lacks strict guarantees. As an
    example, many XML- and JSON-documents belong into this category.
    \parencite{Li2008, Gandomi2015}.
  \item[Mixed]
    A combination of multiple categories of structuredness. For instance, a data format could include
    both structured fields (e.g. time, location), but also unstructured data
    fields (e.g. ``description'' field writted in natural language).
\end{description}

The two most commonly used data formats are JSON (Javascript Object Notation)
and XML (Extensible Markup Language). Both of the formats can contain data in
either semi-structured or mixed manner \parencite{Kleppmann2017}. In this
thesis, the scope will be limited to only briefly introducing the JSON notation.

JSON is a flexible data format, providing support for encoding both
key-value objects and lists. \autoref{fig:json-example} shows a structure
of a simple JSON document. Key-value objects are surrounded with curly braces
and can contain an arbitrary number of key-value pairs. List structures, on
the other hand, are indicated with brackets. All text strings are to be
surrounded with quotation marks, but numerical values do not have a similar
requirement. \parencite{Kleppmann2017}

\begin{figure}[h]
\begin{lstlisting}[language=json,firstnumber=1]
{
  "name": "Elli Esimerkki",
  "address": "Katuosoite 123",
  "age": 52,
  "children": [
    {
      "name": "Erkki Esimerkki"
    },
    {
      "name": "Emma Esimerkki"
    }
  ]
}
\end{lstlisting}
\caption[JSON Example]{Example of a JSON document}
\label{fig:json-example}
\end{figure}

The key-value property of JSON allow nesting of data in a tree-like
structure. For instance, in the example \autoref{fig:json-example}, children
could have additional children of their own with each of them having their
unique details and attributes. The depth of nested data is only limited by
the capability and performance of the software reading the JSON document.
\parencite{Kleppmann2017}

\subsection{Properties of big data}

\noindent
With the production and storage of increasing amount of data, the key
competitive advantage for many organizations can now be derived from the
capability of managing and finding insights from those otherwise
overwhelmingly large and complex pools of data. Sensors, finance, accounting
and user activity are some of the most major sources of this data, and
contribute to the phenomenon also known as ``data deluge''
\parencite{Yin2015}, a downpour of data capable of drowning IT infrastructure
that is not properly prepared for it. Similarly, any service that acts as an
intermediary for such data at a similar scale has to respond to constraints
set by big data.

In this thesis, we will use the definition of big data by \textcite{Assuncao2015}
However, it should be noted that there is not a single universal metric that
can be used to determine whether a certain data set falls into the dimensions
of big data as it depends on the size, industry, and location of the
organization, and how the definition of big data changes over time
\parencite{Gandomi2015}. According to some definitions, even exceptionally
large, but otherwise mundane files (e.g. a 40MB PowerPoint presentation)
could be considered to be big data \parencite{Zaslavsky2013}.

\begin{description}
\item[Variety]
The amount of different types of data. A large variety of data formats translates
into more complex requirements for software used to process the data, as logic for
handling different formats must be included \parencite{Yin2015}.
\item[Velocity]
The rate in which the data arrives and is processed, often varying
depending on the data source, processing, and network capabilities of the big
data systems \parencite{Assuncao2015}. Processing capability is required in
order to transform data into another format or otherwise refine it to 
some other form, while network limits the total bandwidth that can be used for
transferring the raw data \parencite{Yin2015}. The highest velocity is
\textbf{real-time} where a stream of data is received, processed and pushed
forward with minimal delays. A step below this is \textbf{near time}, which
is similar to real-time, but with minor delays. Lastly, \textbf{batches}
process the data in large chunks, which leads to a noticeable delay.
\parencite{Assuncao2015}
\item[Volume]
The total size of data. The larger the data, the more there is overhead in
moving data from place to place. This introduces the performance benefits of
\textit{data locality}. With larger data sets it might be preferable to process
the data as close as possible to the data source in terms of network distance.
This maintains the ratio between the time it takes to transfer data, and
the time it takes to process the data.
\item[Veracity]
How well the data can be trusted. As an example, data on subjective opinions
of people might not objectively correct (e.g. customer reviews, feedback),
but still provide valuable information \parencite{Gandomi2015}. In the
context of DaaS and data markets, veracity indicates the trustworthiness of
specific data sources.
\item[Value]
The value of data in comparison to its volume (\textit{``value density''}).
big data typically has low value density, but has the capability of being
transformed to high value with enough volume \parencite{Gandomi2015}.
However, the metric of value also depends on the organization's capability of
finding value from data \parencite{Assuncao2015}.
\end{description}

\subsection{Stream Processing}

\noindent
Stream processing is an approach where data is processed instantly as it
arrives (\autoref{fig:stream-model}) as opposed to batch processing where
data is processed in chunks or intervals (\autoref{fig:batch-model}).
Traditionally, gathering and processing data from multiple different sources
has been facilitated using ETL (Extract, Transform, Load) tools that move
data to data warehouses in a batch-type fashion. Stream processing systems
provide an alternative to the traditional model \parencite{Kleppmann2017}.

\begin{figure}[h]
  \includegraphics[width=\textwidth]{kuviot/batch-model.png}
\caption[Batch processing]{Batch processing \parencite{Allen2015}}
\label{fig:batch-model}
\end{figure}

In many real-world applications, such as financial transactions and IoT
devices, data is created in continuous streams of events. An event represents
a single, self-contained object that contains details about something that
has happened at some point of time \parencite{Kleppmann2017}. Although such
data could also be processed in batches, in some cases it would be more
appropriate to process it without delays. Stream processing addresses some
drawbacks of processing analytics in batches: When using batches, the data is
never processed real-time, which affects the timeliness of the discovered
insights. This becomes a problem with ``perishable'' data: The longer it
takes to analyze data points, the less value they provide
\parencite{Flannagan2016}. Furthermore, stream processing can be used to
solve issues of scalability: Handling data at scale for systems such as
airline ticket booking or retail systems is difficult because they rely on
multiple different and distributed systems that might end up in conflicting
states. Stream processing can be used to solve some of the problems as the
abstraction of events provides capabilities for resolving those conflicts.
\parencite{Friedman2016}.

\begin{figure}[h]
  \centering
  \includegraphics[height=4.5cm]{kuviot/stream-model.png}
\caption[Stream processing]{Stream processing \parencite{Allen2015}}
\label{fig:stream-model}
\end{figure}

Ideas behind stream processing are partially related to an earlier technology
called Complex Event Processing (CEP) \textcite{Narkhede2017}. CEP was developed
in the 1990s for analyzing streams with certain patterns that were defined
with query languages such as SQL. Unlike traditional databases that would
respond with a one-time response, the queries would define channels with
potentially infinite events. Although the concepts are similar to each
other, the main difference is found from the analytics part of stream
processing: Instead of discovering specific event chains, stream processing
is more focused in statistics and averages. However, features such as
defining streams with SQL are being adapted by modern stream processing
frameworks. Some of those stream processing frameworks include Apache Samza
that introduced SamzaSQL in 2016 \parencite{Pathirage2016}, Apache Kafka that
released their preview of KSQL in 2017 \parencite{Akidau2018}, and Apache
Flink that features a CEP library as a part of the framework
\parencite{Friedman2016}. \parencite{Kleppmann2017}

There are three possible ways to use stream data sources: Firstly, The stream
can be written into different types of storages (e.g. databases, caches)
where it can be used as the application state. This can involve updating a
traditional database, or taking an advantage of a technique called event
sourcing that provides an alternative for storing state. In a banking
application, for instance, the state could include user accounts, balances
and transfers. With event sourcing, however, that same state could also be
contained as a series of events that could be used to form the state itself
on demand. Using the same banking example, a balance of a bank account could
be kept as a single column of data in a database that is updated at each
transaction. Alternatively, the balance could be calculated every time on the
fly by summing together all the events where money is transferred to an
account and negating the sum of transactions where money was moved away from
the account. Another approach is taking an advantage of both databases and
event sources: A database can be used to reflect the current state while also
retaining the log of events for other purposes. Second, the events of
stream data can be sent directly to users in form of real-time dashboard or
notifications. And third, the input stream can be transformed with other
streams to form new kinds of output streams; an approach that can also be
applied to data marketplaces and hybrid data products.
\parencite{Friedman2016,Kleppmann2017}

According to \textcite{Stonebraker2005}, there are eight rules that stream
processing systems must follow:

\begin{description}
  \item[Keep the Data Moving]
  The system must be able to handle message processing
  with minimal latency. This means avoiding bottlenecks in the processing
  path (e.g. costly storage operations). The messages should be processed "on
  the fly" as they arrive.
  \item[Query using SQL on Streams]
  It's desirable to process real-time data using a high-level language like
  SQL instead of relying on lower application programming languages (such as
  Java and C++) as it results in shorter development cycles and lower
  maintenance costs. As SQL is primarily intended for use with relational
  databases, it needs additional features to fulfill needs of stream
  processing systems.
  \item[Handle stream imperfections]
  The stream system must be able to handle situations where the stream data
  might arrive late, out of order, or with some data missing entirely.
  Although some processing might rely on waiting for all data to arrive, the
  data might never arrive, leading to a standstill. Therefore every function
  in the stream system should have a time out period that allows continuing
  even with incomplete data.
  \item[Generate predictable outcomes]
  The output of the stream processing system should be deterministic and
  repeatable. In other words, providing the same input data in the same order
  should always lead to the same results. In addition to ensuring the
  correctness of the system, deterministic and repeatable results are
  necessary when the system has to recover from a failure and the same
  application state before the crash has to be replicated.
  \item[Integrate stored and streaming data]
  The stream processing system must be capable of combining stored and stream
  data together. This is commonly used in applications where messages that
  happened in the past are used together with current data (e.g. fraud
  detection). In addition to maintaining history in the format it arrived in,
  another approach is to maintain signatures of ``normal'' and unusual data
  based on past behavior as a summarization of earlier messages. Additionally,
  stored data is useful during the development of applications, allowing
  the developer to conduct repeatable experiments with past data before
  switching to live data. It should be noted that interfacing with external
  databases can add notable latency to the pipeline, and would conflict with
  the rule of ``Keeping the data moving''.
  \item[Guarantee data safety and availability]
  For the sake of reliability and fault tolerance, the stream processing
  system must, by design, be highly available. In a case of hardware failure,
  the stream processing system should be able to restore itself
  with little to no delay. Longer downtimes are
  unacceptable for real-time processing systems, as the system would not be
  ``real-time'' for the period of the downtime.
  \item[Partition and scale application automatically]
  It should be possible to distribute the stream processing system over
  multiple machines due to the scalability and price-performance benefits
  offered by clusters built with commodity hardware. The system should be
  able to scale automatically and transparently to available machines
  depending on the system load. Additionally, the stream processing system
  should support multiple threads to take full advantage of multiple processor
  cores in modern hardware.
  \item[Process and respond instantaneously]
  The processing system must be able to process high volumes of data with
  very low latency, typically in the range of microseconds to milliseconds on
  commodity off-the-shelf hardware. This requires that all the system
  components are designed with performance in mind to prevent bottlenecks.
  However, the performance of the system might also vary depending on the
  type of workload, making this rule workload-dependent.
\end{description}

\section{Summary}

\noindent
Online data marketplaces are Data as a Service (DaaS) platforms that facilitate
trading of data via internet. DaaS provides a framework of standard formats and
interfaces that enable operating data products through an API. There exists
three general categories of DaaS platforms: \textit{Generic} platforms with
multiple independent data sets, \textit{specialized} platforms that are centered
around a single (or limited amount of) domain specific dataset(s), and
\textit{hybrid} platforms that may provide an access to specialized datasets in
addition to a generic API. \parencite{Vu2012}

As the data products on a DaaS platform might extend to the scale of big data, a
DaaS platform must be capable of handling big data. Technologies required by a
DaaS platform include \textit{data modeling tools} for humans to handle the
complexity of data, \textit{common query language and API} to access and
manipulate the data programmatically, \textit{massive scale data management} to
respond to challenges of big data, and \textit{data cleansing and processing
  technologies} to process, clean, and transform data if necessary.
\parencite{Chen2011}

Data handled by a DaaS platform can have different properties. The level of
structuredness of data determines how easily it is handled with automated tools.
The two most commonly used data formats, JSON and XML, can contain both
semi-structured and mixed data. JSON is a flexible data format that can contain
data in a tree-like structure using key-value pairs.
\parencite{Assuncao2015,Kleppmann2017}

Properties of big data include \textit{variety} that describes the diversity of
processed data, \textit{velocity} as the speed or throughput of data,
\textit{volume} representing the total size of data, \textit{veracity}
determining the trustworthiness of data, and \textit{value} of data in terms of
density of valuable information in comparison to noise \parencite{Assuncao2015}.
Data can be processed using \textit{batch or stream processing}, with latter
approach being the more viable choice for real-time use cases
\parencite{Kleppmann2017,Flannagan2016}. Stream processing systems must
\textit{keep data moving}, \textit{be able to use SQL on Streams},
\textit{handle stream imperfections}, \textit{generate predictable outcomes},
\textit{integrate stored and streaming data}, \textit{guarantee data safety and
  availability}, \textit{partition and scale application automatically}, and
\textit{process and respond instantaneously} \parencite{Stonebraker2005}.
